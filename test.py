#!/usr/bin/python

# This is my exploration in pyopengl land

import sys, os

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL import Tk

WINDOW_SIZE = (800, 600)
WINDOW_TITLE = "Whee!"

def init():

    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGB|GLUT_DEPTH|GLUT_DOUBLE)
    glutInitWindowSize(WINDOW_SIZE[0], WINDOW_SIZE[1])
    glutCreateWindow(WINDOW_TITLE)

    glClearColor(0.0, 0.0, 0.0, 0.0)

    return

def display():

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glColor3f(0.0, 1.0, 0.0);
    glRectf(-0.75,0.75, 0.75, -0.75);
    glutSwapBuffers()

    return

def run():
    glutDisplayFunc(display)
    glutMainLoop()

def main():

    init()

    run()

    return

if __name__ == '__main__':
    main()
