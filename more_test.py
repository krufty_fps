#!/usr/bin/python

# PyOpenGL + Pygame

CONFIG = 'game_config.xml'

WINDOW_SIZE = (640, 480)
WINDOW_CAPTION = "Testing stuff"

BROADCAST = 50030
BROADCAST_BIND = 50031
MESSAGE = 40030

BUFSIZE = 120

TIMEOUT = .1

# this will change

SERVERREQUEST = "i_want_server"
SERVEROFFER = "Want_server?"
SERVERKILL = "DIE_server!!"
YOUTHERE = "you_there?"
IMHERE = "yeah,i'm_here"
WANTIN = "i_want_in"
YOUREIN = "urine"
IMOUT = "i_leave"
GETLIST = "get_list"
LIST = "peoples_on_server"
SOMEONEJOINED = "dude,someone_joined"
SOMEONELEFT = "someone_left"
YOUROUT = "get_lost_punk"
LETTER = "listen_to_me"

the_functions = {}
in_script = 0

import os, sys
from os import *
from os.path import *

import threading
from socket import *
from xml.dom.minidom import parse, parseString
from threading import Lock
from signal import *
from zipfile import *

import pygame
from pygame import *

import pgu
from pgu import gui as pgui

import lamina

import OpenGL
import OpenGL.GL
from OpenGL.GL import *
import OpenGL.GLU
from OpenGL.GLU import *

import StringIO

the_event_receiver = 0

global the_engine
global the_client

class Client:

    def __init__(self, params):

        self.params = params

        self.outgoing = []
        self.incoming = []

        self.outgoing_lock = Lock()
        self.incoming_lock = Lock()

        self.broadcast = socket(AF_INET, SOCK_DGRAM)
        self.broadcast.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

        self.message = socket(AF_INET, SOCK_DGRAM)

        self.message_lock = Lock()

        self.members = []
        self.servers = {}

        self.current_server = []
        self.requested_server = ()

        self.messages_in = []

        self.commands = CommandLine()

        self.messages_in_lock = Lock()

        if (params.has_key('broadcast_bind')):
            self.broadcast.bind(((''), params['broadcast_bind']))
        else:
            print "Client.__init__(): No broadcast port number chosen, using", BROADCAST_BIND
            self.broadcast.bind(((''), BROADCAST_BIND))

        if (params.has_key('message_port')):
            self.message.bind(((''), params['message_port']))
        else:
            print "Client.__init__(): No broadcast port number chosen, using", MESSAGE
            self.message.bind(((''), MESSAGE))

    def run(self):

        print "Client.run(): Making threads..."

        self.send_thread = threading.Thread(target=self.send)
        self.receive_thread = threading.Thread(target=self.receive)
        self.process_in_thread = threading.Thread(target=self.process_in)
        self.process_out_thread = threading.Thread(target=self.process_out)

        self.send_thread.start()
        self.receive_thread.start()
        self.process_in_thread.start()
        self.process_out_thread.start()

        self.send_thread.join()
        self.receive_thread.join()
        self.process_in_thread.join()
        self.process_out_thread.join()

        print "Client.run(): Closed threads"

        return

    def send(self):

        print "Client.send() beginning..."

        while (the_engine.running):

            if (len(self.outgoing)):

                self.outgoing_lock.acquire()
                print "Client.send(): Acquired outgoing_lock"

                data_out = self.outgoing.pop(0)

                self.outgoing_lock.release()
                print "Client.send(): Released outgoing_lock"

                if type(data_out[1]) != type(()):
                    data_out[1] = (data_out[1][0], data_out[1][1])

                if data_out[1][0] != '<broadcast>':
                    self.message_lock.acquire()
                    self.message.sendto(data_out[0], data_out[1])
                    self.message_lock.release()
                else:
                    self.broadcast.sendto(data_out[0], data_out[1])
                print "Client.send(): Sent data", data_out
        return

    def receive(self):

        print "Client.receive() beginning..."

        self.message.settimeout(TIMEOUT)

        while (the_engine.running):

            print "debug, no?"

            try:
                self.message_lock.acquire()
                data_in = self.message.recvfrom(BUFSIZE)
                self.message_lock.release()
            except:
                data_in = ()

            if (data_in != ()):
                self.incoming_lock.acquire()
                self.incoming.append(data_in)
                self.incoming_lock.release()

    def process_in(self):

        print "Client.process_in() beginning..."

        global the_engine

        while (the_engine.running):

            if (len(self.incoming)):
                # data in = ()

                self.incoming_lock.acquire()
                print "Client.process_in(): Acquired incoming_lock"

                data_in = self.incoming.pop(0)

                self.incoming_lock.release()
                print "Client.process_in(): Released incoming_lock"

                if (data_in[0][:len(YOUTHERE)] == YOUTHERE):
                    print "Client.process_in(): Received YOUTHERE, responding with IMHERE"

                    response = [IMHERE, data_in[1]]

                    self.outgoing_lock.acquire()
                    print "Client.process_in(): Acquired outgoing_lock"

                    self.outgoing.append(response)

                    self.outgoing_lock.release()
                    print "Client.process_in(): Released outgoing_lock"

                if (data_in[0][:len(SERVEROFFER)] == SERVEROFFER):
                    print "Client.process_in(): Received SERVEROFFER"

                    split_strings = data_in[0].split()
                    self.servers[split_strings[2]] = [data_in[0][0], int(split_strings[1])]

                    event = [SERVEROFFER, {'server_name': split_strings[2]}]

                    the_engine.events.addEvent(event)

                if (data_in[0][:len(YOUREIN)] == YOUREIN):
                    if (data_in[1] == self.requested_server):
                        self.current_server = data_in[1]
                        self.requested_server = ()

                        event = [YOUREIN]
                        data = {'server': data_in[1]}
                        event.append(data)

                        the_engine.events.addEvent(event)

                        print "Client.process_in(): Received YOUREIN, joined server"

                if (data_in[0][:len(LIST)] == LIST):
                    print "Client.process_in(): Received LIST"

                    if (data_in[1] == self.current_server):
                        split_strings = data_in[0].split()

                        self.members = []

                        for string in split_strings:
                            if string != LIST:
                                self.members.append(string)

                        event = [LIST]
                        data = {'names': self.members}
                        event.append(data)

                        the_engine.events.addEvent(event)

                if (data_in[0][:len(SOMEONEJOINED)] == SOMEONEJOINED):
                    print "Client.process_in(): Received SOMEONEJOINED"

                    if (data_in[1] == self.current_server):
                        left_member = ''

                        for member in self.members:
                            if member == data_in[0][len(SOMEONEJOINED) + 1:]:
                                left_member = member

                        if left_member != '':
                            self.members.remove(left_member)

                if (data_in[0][:len(YOUROUT)] == YOUROUT):
                    print "Client.process_in(): Received YOUROUT"

                    if (data_in[1] == self.current_server):
                        self.current_server = []

                        event = [YOUROUT]
                        data = {}
                        event.append(data)

                        the_engine.events.addEvent(event)

                if (data_in[0][:len(LETTER)] == LETTER):
                    print "Client.process_in(): Received LETTER"

                    if (data_in[1] == self.current_server):
                        split_strings = data_in[0].split(':')

                        the_message_data = data_in[0][this_string.find(":", len(LETTER) + 1) + 1:]
                        the_message_origin = split_strings[1]

                        self.messages_in_lock.acquire()
                        the_message = (the_message_data, the_message_origin)
                        self.messages_in.append(the_message)
                        self.messages_in_lock.release()

                        event = [LETTER]
                        data = {'message': the_message_data}
                        event.append(data)

                        the_engine.events.addEvent(event)

    def process_out(self):

        print "Client.process_out(): beginning..."

        while (the_engine.running):

            if (self.commands.size()):

                # {'type': ..., 'data': ...}

                command = self.commands.getCommand()

                if command['type'] == SERVERKILL:

                    if self.current_server != ():
                        out_message = [''.join([SERVERKILL, ' ']), self.current_server]

                        if command.has_key('password'):
                            out_message[0] += command['password']

                        self.outgoing_lock.acquire()
                        self.outgoing.append(out_message)
                        self.outgoing_lock.release()

                if command['type'] == SERVERREQUEST:

                    out_message = []

                    out_message.append(''.join([SERVERREQUEST, " ", str(self.params['message_port'])]))
                    out_message.append(['<broadcast>', self.params['broadcast_port']])

                    self.outgoing_lock.acquire()
                    self.outgoing.append(out_message)
                    self.outgoing_lock.release()

                if command['type'] == GETLIST:

                    if self.current_server != ():

                        out_message = [GETLIST, self.current_server]

                        self.outgoing_lock.acquire()
                        self.outgoing.append(out_message)
                        self.outgoing_lock.release()

                if command['type'] == IMOUT:

                    if self.current_sever != ():

                        out_message = [IMOUT, self.current_server]

                        self.outgoing_lock.acquire()
                        self.outgoing.append(out_message)
                        self.outgoing_lock.release()

                if command['type'] == LETTER:

                    if self.current_server != ():

                        out_message = [LETTER + ':', self.current_server]

                        for dest in command['destinations']:
                            if dest in self.members:
                                out_message[0] += " " + dest

                        out_message[0] += ":" + command['message']

                        self.outgoing_lock.acquire()
                        self.outgoing.append(out_message)
                        self.outgoing_lock.release()

        return

class QueueLock:
    def __init__(self):

        self.items = []
        self.items_lock = Lock()

    def size(self):

        return len(self.items)

    def getItem(self):

        self.items_lock.acquire()
        the_item = self.items.pop(0)
        self.items_lock.release()

        return the_item

    def addItem(self, new_item):

        self.items_lock.acquire()
        self.items.append(new_item)
        self.items_lock.release()

        return

class EventReceiver(QueueLock):

    def getEvent(self):
        return self.getItem()
    def addEvent(self, event):
        self.addItem(event)
        return

class CommandLine(QueueLock):

    def getCommand(self):
        return self.getItem()
    def addCommand(self, command):
        self.addItem(command)
        return

class Engine:

    def __init__(self, config):

        self.config = config
        self.running = 1

        global the_engine
        the_engine = self

        video_flags = OPENGL|DOUBLEBUF

        pygame.init()
        self.display = pygame.display.set_mode(config['window_size'], video_flags)

        self.font = pygame.font.SysFont("default", 18)
        self.fontBig = pygame.font.SysFont("default", 24)
        self.fontSub = pygame.font.SysFont("default", 20)
        self.theme = pgui.Theme(['test_theme', 'gray', 'default'])
#        self.theme = pgui.Theme('gray')
#        self.theme = pgui.Theme('default')

        self.resize((config['window_size']))

        glShadeModel(GL_SMOOTH)
        glClearColor(self.config['background_color'][0],
                     self.config['background_color'][1],
                     self.config['background_color'][2],
                     self.config['background_color'][3])
        glClearDepth(1.0)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

        pygame.display.set_caption(config['window_title'])

        self.gui_screen = lamina.LaminaScreenSurface()

        self.app = pgui.App(theme=self.theme)
        self.app._screen = self.gui_screen.surf
        self.main_container = pgui.Container(width=config['window_size'][0])

        self.files = {}

        self.events = EventReceiver()

        # self.files is a map of filename to file

        # The file has this:
        #  {parent, [children], {object_defs},
        #  {menu_defs}, {object_instances}, {menu_instances}}

        self.zipfiles = []

        for file in listdir(self.config['pack_dir']):
            if (is_zipfile(os.path.join(self.config['pack_dir'], file))):
                self.zipfiles.append(ZipFile(os.path.join(self.config['pack_dir'], file)))

        self.running = self.addFile(self.config['init'], '')
        self.app.init(self.main_container)

        self.ticks = pygame.time.get_ticks()
        self.frames = 0

        if self.running == 0:
            print "Engine.__init__(): Failed adding initial file"

    def run(self):

        while self.running:

            self.process_events()

            self.draw()

            self.updateFPS()

            pygame.display.flip()

        return

    def updateFPS(self):

        self.oldticks = self.ticks
        self.ticks = pygame.time.get_ticks()

        self.frames += 1

        self.fps = (self.frames * 1000) / (self.ticks - self.oldticks) / 1000

        print 'DEBUG is', self.config['window_title']
        pygame.display.set_caption(''.join([self.config['window_title'], ' ', str(self.fps)]))

        return

    def process_events(self):

        change = 0

        for e in pygame.event.get():
            if e.type == QUIT:
                self.running = 0
                print "Should be quitting..."

            self.app.event((e))
            change = self.app.update(self.gui_screen.surf)

            if (change != 0):
                self.gui_screen.refresh(change)

            new_event = self.convertEvent(e)
            self.events.addEvent(new_event)

        iterating = 1
        while iterating:

            if self.events.size():
                new_event = self.events.getEvent()
            else:
                iterating = 0

            if not iterating:
                return

            if iterating:
                for file in self.files.keys():

                    for name in self.files[file]['object_instances'].keys():

                        self.files[file]['object_instances'][name].event(new_event[0], new_event[1])

        return

    def convertEvent(self, e):

        data = {}

        if e.type == QUIT: 
            data['none'] = ''

        if e.type == ACTIVEEVENT: 
            data['gain'] = e.gain 
            data['state'] = e.state

        if e.type == KEYDOWN: 
            data['unicode'] = e.unicode 
            data['key'] = e.key 
            data['mod'] = e.mod

        if e.type == KEYUP: 
            data['key'] = e.key 
            data['mod'] = e.mod

        if e.type == MOUSEMOTION:	 
            data['pos'] = e.pos 
            data['rel'] = e.rel 
            data['buttons'] = e.buttons

        if e.type == MOUSEBUTTONUP: 
            data['pos'] = e.pos 
            data['button'] = e.button

        if e.type == MOUSEBUTTONDOWN: 
            data['pos'] = e.pos 
            data['button'] = e.button

        if e.type == JOYAXISMOTION: 
            data['joy'] = e.joy 
            data['axis'] = e.axis 
            data['value'] = e.value

        if e.type == JOYBALLMOTION: 
            data['joy'] = e.joy 
            data['ball'] = e.ball 
            data['rel'] = e.rel

        if e.type == JOYHATMOTION: 
            data['joy'] = e.joy 
            data['hat'] = e.hat 
            data['value'] = e.value

        if e.type == JOYBUTTONUP: 
            data['joy'] = e.joy 
            data['button'] = e.button

        if e.type == JOYBUTTONDOWN: 
            data['joy'] = e.joy 
            data['button'] = e.button

        if e.type == VIDEORESIZE: 
            data['size'] = e.size 
            data['w'] = e.w 
            data['h'] = e.h

        if e.type == VIDEOEXPOSE: 
            data['none'] = ''

        if e.type == USEREVENT: 
            data['code'] = e.code

        type = ''

        if e.type == QUIT: type = "QUIT"
        if e.type == ACTIVEEVENT: type = "ACTIVEEVENT"
        if e.type == KEYDOWN: type = "KEYDOWN"
        if e.type == KEYUP: type = "KEYUP"
        if e.type == MOUSEMOTION    : type = "MOUSEMOTION"
        if e.type == MOUSEBUTTONUP: type = "MOUSEBUTTONUP"
        if e.type == MOUSEBUTTONDOWN: type = "MOUSEBUTTONDOWN"
        if e.type == JOYAXISMOTION: type = "JOYAXISMOTION"
        if e.type == JOYBALLMOTION: type = "JOYBALLMOTION"
        if e.type == JOYHATMOTION: type = "JOYHATMOTION"
        if e.type == JOYBUTTONUP: type = "JOYBUTTONUP"
        if e.type == JOYBUTTONDOWN: type = "JOYBUTTONDOWN"
        if e.type == VIDEORESIZE: type = "VIDEORESIZE"
        if e.type == VIDEOEXPOSE: type = "VIDEOEXPOSE"
        if e.type == USEREVENT: type = "USEREVENT"

        return [type, data]

    def draw(self):

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()

        # draw stuff

        glLoadIdentity()
        self.gui_screen.display()

    def resize(self, (width, height)):
        if height==0:
            height=1
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45, 1.0*width/height, 0.1, 100.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        return

    def addFile(self, filename, parent):

      # Because I have alot of nesting in this function,
      # I am using 2 space formatting

      for zipfile in self.zipfiles:

        for file in zipfile.namelist():

          if (file == filename):
            dom = parseString(zipfile.read(file))

            object_instances_d = []
            menu_instances = []

            if dom.childNodes[0].nodeName == 'game':
              self.files[filename] = {
                  'object_defs': {},
                  'children': [],
                  'parent': '',
                  'object_instances': {},
                  'menu_instances': {},
                  'menu_defs': {}}

              for node in dom.childNodes[0].childNodes:

                if (node.nodeName == 'def'):

                  for sub_node in node.childNodes:

                    if (sub_node.nodeName == 'object'):

                      temp_object_def = {}

                      for suber_node in sub_node.childNodes:

                        if (suber_node.nodeName == 'name'):
                          if len(suber_node.childNodes):
                            temp_object_def['name'] = suber_node.childNodes[0].nodeValue

                        if (suber_node.nodeName == 'script'):
                          if len(suber_node.childNodes):
                            temp_object_def['script'] = suber_node.childNodes[0].nodeValue

                        if (suber_node.nodeName == 'tangible'):
                          if len(suber_node.childNodes):
                            temp_object_def['tangible'] = suber_node.childNodes[0].nodeValue

                        if (suber_node.nodeName == 'type'):
                          if len(suber_node.childNodes):
                            temp_object_def['type'] = suber_node.childNodes[0].nodeValue

                      self.files[filename]['object_defs'][temp_object_def['name']] = temp_object_def
                      temp_object_def = {}

                    if (sub_node.nodeName == 'menu'):

                      temp_menu_def = {}
                      temp_menu_def['elements'] = {}

                      temp_element_set = {}

                      for suber_node in sub_node.childNodes:

                        if (suber_node.nodeName == 'name'):
                          temp_menu_def['name'] = suber_node.childNodes[0].nodeValue

                        if (suber_node.nodeName == 'elements'):

                          for suberer_node in suber_node.childNodes:

                            if (suberer_node.nodeType != 3 and suberer_node.nodeType != 8):
                              if (suberer_node.hasAttribute('name')):
                                temp_element_set['name'] = suberer_node.getAttribute('name')

                              if (suberer_node.hasAttribute('x')):
                                temp_element_set['x'] = int(suberer_node.getAttribute('x'))

                              if (suberer_node.hasAttribute('y')):
                                temp_element_set['y'] = int(suberer_node.getAttribute('y'))

                              if (suberer_node.hasAttribute('width')):
                                temp_element_set['width'] = int(suberer_node.getAttribute('width'))

                              if (suberer_node.hasAttribute('height')):
                                temp_element_set['height'] = int(suberer_node.getAttribute('height'))

                              if (suberer_node.hasAttribute('parent')):
                                temp_element_set['parent'] = suberer_node.getAttribute('parent')

                              if (suberer_node.hasAttribute('target')):
                                temp_element_set['target'] = suberer_node.getAttribute('target')

                              if (suberer_node.hasAttribute('text')):
                                temp_element_set['text'] = suberer_node.getAttribute('text')

                              temp_element_set['type'] = suberer_node.nodeName
                              temp_menu_def['elements'][temp_element_set['name']] = temp_element_set
                              temp_element_set = {}

                      self.files[filename]['menu_defs'][temp_menu_def['name']] = temp_menu_def

                      temp_menu_def = {}
                      temp_element_set = {}

                if (node.nodeName == 'instance'):

                  for sub_node in node.childNodes:

                    if (sub_node.nodeName == 'object'):
                      object_instances_d.append(sub_node.childNodes[0].nodeValue)

                    if (sub_node.nodeName == 'menu'):
                      menu_instances.append(sub_node.childNodes[0].nodeValue)

            if (parent != ''):
              self.files[parent]['children'].append(filename)
            self.files[filename]['parent'] = parent

            # create instances

            for menuname, menu_data in self.files[filename]['menu_defs'].iteritems():
              self.files[filename]['menu_instances'][menuname] = Menu(menu_def=menu_data)

            for menuname in menu_instances:
              if (self.files[filename]['menu_instances'].has_key(menuname)):
                self.files[filename]['menu_instances'][menuname].show()

              for file in self.files.keys():
                if self.files[file]['menu_instances'].has_key(menuname):
                  self.files[file]['menu_instances'][menuname].show()

            for objectname in object_instances_d:
              self.addObject(objectname)

            return 1
      return 0
    # ending 2-space formatting

    def removeFile(self, filename):

        if (self.files.has_key(filename)):

            for child in self.files[filename]['children']:
                self.removeFile(child)

            del self.files[filename]['children']

            parent = self.files[filename]['parent']
            self.files[parent].pop(self.files[parent]['children'].index(filename))

            del self.files[filename]

        return

    def hideAllMenus(self):

        for file, contents in self.files.iteritems():
            for name, menu in contents['menu_instances'].iteritems():
                menu.hide()

        return

    def showMenu(self, menu_name):

        for file, contents in self.files.iteritems():
            for name, menu in contents['menu_instances'].iteritems():
                if contents['menu_instances'].has_key(menu_name):
                    contents['menu_instances'][menu_name].show()

        return

    def hideMenu(self, menu_name):

        for file, contents in self.files:
            for name, menu in contents['menu_instances'].iteritems():
                if contents['menu_instances'].has_key(menu_name):
                    contents['menu_instances'][menu_name].hide()

        return

    def setMenu(self, menu_name):

        self.hideAllMenus()
        self.showMenu(menu_name)

        return

    def addObject(self, objectname):

        for file in self.files.keys():
            if self.files[file]['object_defs'].has_key(objectname):
                self.files[file]['object_instances'][objectname] = GameObject(
                    self.files[file]['object_defs'][objectname])

        return

    def removeObject(self, objectname):

        for file in self.files.keys():
            if self.files[file]['object_defs'].has_key(objectname):
                del self.files[file]['object_instances'][objectname]

        return

    def getImage(self, filename):

        the_buf = pygame.image.load(self.getStringIOFile(filename))

        return the_buf

    def getStringIOFile(self, filename):

        for zip_file in self.zipfiles:
            for file in zip_file.namelist():

                if (filename == file):

                    the_string_io = StringIO.StringIO()
                    print >>the_string_io, zip_file.read(file)
                    the_string_io.seek(0)

                    return the_string_io
        return ''

    def getStringData(self, filename):
        for zip_file in self.zipfiles:
            for file in zip_file.namelist():

                if (filename == file):
                    return zip_file.read(file)
        return ''

class Menu:

    def __init__(self, menu_def):

        self.menu_def = menu_def
        self.widgets = {}

        for name, element in menu_def['elements'].iteritems():

            multiplier_x = float(the_engine.config['window_size'][0]) / 100.00
            multiplier_y = float(the_engine.config['window_size'][1]) / 100.00

            dx = float(element['x']) * multiplier_x
            dy = float(element['y']) * multiplier_y

            dwidth = float(element['width']) * multiplier_x
            dheight = float(element['height']) * multiplier_y

            if (element['type'] == 'button'):
                self.widgets[name] = pgui.Button(element['text'].encode(), width=dwidth, height=dheight)
                self.widgets[name].connect(pgui.CLICK, self.clicked, name)

            if (element['type'] == 'image'):
                self.widgets[name] = pgui.Image(the_engine.getImage(element['text']))

            if (element['type'] == 'label'):
                self.widgets[name] = pgui.Label(element['text'])

            if (element['type'] == 'listbox'):
                self.widgets[name] = pgui.List(width=dwidth, height=dheight)

            if (self.widgets.has_key(name)):
                self.widgets[name].resize(width=dwidth, height=dheight)
            else:
                print "Menu.__init__(): Widget type", element['type'], " not implemented yet, skipping."

        self.hidden = 1

    def show(self):

        global the_engine

        multiplier_x = float(the_engine.config['window_size'][0]) / 100.00
        multiplier_y = float(the_engine.config['window_size'][1]) / 100.00

        if (self.hidden == 1):
            for name, element in self.menu_def['elements'].iteritems():
                dx = float(element['x']) * multiplier_x
                dy = float(element['y']) * multiplier_y

                if self.widgets.has_key(name):
                    the_engine.main_container.add(self.widgets[name], dx, dy)

        self.hidden = 0

        return

    def hide(self):

        global the_engine

        if (self.hidden != 1):
            for name, element in self.menu_def['elements'].iteritems():

                if (self.widgets.has_key(name)):
                    the_engine.main_container.remove(self.widgets[name])

        self.hidden = 1

        return

    def clicked(self, button_name):

        if self.menu_def['elements'].has_key(button_name):

            if self.menu_def['elements'][button_name]['target'] != '':
                exec(self.menu_def['elements'][button_name]['target'])

            the_engine.events.addEvent(['BUTTON_CLICKED',
                                       {'name': button_name, 'menu_name': self.menu_def['name']}])

        return

class GameObject:

    def __init__(self, dmold):

        self.mold = dmold
        global the_functions
        global in_script

        if self.mold['script'] != '':

            in_script = 1

            the_file = the_engine.getStringData(self.mold['script'])
            if the_file != '':
                exec(the_file)

            in_script = 0
            self.my_functions = the_functions

            the_functions = {}

    def event(self, event, data):

        if self.my_functions.has_key(event):

            self.my_functions[event](data)

        return

def main():
    # parse command line stuff

    if (len(sys.argv) > 1):
        config = sys.argv[1]
    else:
        print "main(): No config specified, using", CONFIG
        config = CONFIG

    configuration = parse_config(config)

    if (configuration.has_key('error') and configuration['error']):
        print "main(): Error in parsing config."

    the_event_receiver = EventReceiver()

    client_params = {'broadcast_port':configuration['broadcast_port'],
                     'broadcast_bind':configuration['broadcast_bind'],
                     'message_port':configuration['message_port'],
                     'name':configuration['name']}

    global the_client
    global the_engine

    the_client = Client(client_params)
    the_engine = Engine(configuration)

    the_client_thread = threading.Thread(target=the_client.run)
#    the_engine_thread = threading.Thread(target=the_engine.run)

    the_client_thread.start()
#    the_engine_thread.start()

    the_engine.run()

    the_client_thread.join()
#    the_engine_thread.join()

    return

def parse_config(filename):
    results = {'error': 1}

    if (exists(filename)):
        dom = parse(filename)
    else:
        print "parse_config():", filename, "doesn't exist."
        return results

    if (dom.childNodes[0].nodeName == 'config'):
        for node in dom.childNodes[0].childNodes:

            if (node.nodeName == 'window_title'):
                results['window_title'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'log'):
                results['log'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'name'):
                results['name'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'font'):
                results['font'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'init'):
                results['init'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'message_port'):
                results['message_port'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'broadcast_port'):
                results['broadcast_port'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'broadcast_bind'):
                results['broadcast_bind'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'fullscreen'):
                results['fullscreen'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'background_color'):

                string_parts = node.childNodes[0].nodeValue.split()
                results['background_color'] = [float(string_parts[0]), float(string_parts[1])]
                results['background_color'].append(float(string_parts[2]))
                results['background_color'].append(float(string_parts[3]))

            if (node.nodeName == 'window_size'):

                string_parts = node.childNodes[0].nodeValue.split()
                results['window_size'] = ((int(string_parts[0]), int(string_parts[1])))

            if (node.nodeName == 'gravity'):

                string_parts = node.childNodes[0].nodeValue.split()
                results['gravity'] = [float(string_parts[0]), float(string_parts[1])]
                results['gravity'].append(float(string_parts[2]))

            if (node.nodeName == 'step_size'):
                results['step_size'] = float(node.childNodes[0].nodeValue)

            if (node.nodeName == 'pack_dir'):
                results['pack_dir'] = node.childNodes[0].nodeValue

    results['error'] = 0

    return results

if __name__ == '__main__':
    main()
