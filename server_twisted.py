#!/usr/bin/python

# This is a twisted version of the server

import os, sys
from os.path import exists

import signal
from signal import *

from xml.dom.minidom import parse, parseString

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.internet import task

CONFIG = 'config.xml'

BROADCAST = 50030
MESSAGE = 40031
PASSWORD = "changethis"
NAME = "default"

ECHO_TIME = 1

ECHO_COUNT = 5 # this is the number of times one can ignore an echo until the server drops you

running = 1

from protocol import *

MULTICAST = '234.0.0.1'

def main():

    # parsing command line options

    if (len(sys.argv) > 1):
        config_file = sys.argv[1]
    else:
        print "main(): No config file specified, using", CONFIG
        config_file = CONFIG

    global parsed_config
    parsed_config = parse_config(config_file)

    if (parsed_config['error']):
        print "main(): Error parsing config, ending"
        return

    signal(SIGINT, handler)

    run(parsed_config)

def run(configuration):

    global running

    if running:
        print "run(): Beginning..."

    global the_messenger
    global the_broadcast

    global members
    members = {}

    global ECHO_COUNT

    if configuration.has_key('echo_number'):
        ECHO_COUNT = configuration['echo_number']
    else:
        print "No echo_number specified in config file, using default of", ECHO_COUNT

    the_broadcast = Broadcast()
    the_messenger = Messenger()

    the_messenger.echo_members = []

    if configuration.has_key('broadcast'):
        reactor.listenMulticast(configuration['broadcast'], the_broadcast)
    else:
        print "Server.__init__(): No broadcast socket number chosen, using", BROADCAST
        reactor.listenMulticast(BROADCAST, the_broadcast)

    if configuration.has_key('message'):
        reactor.listenUDP(configuration['message'], the_messenger)
    else:
        print "Server.__init__(): No message socket number chosen, using", MESSAGE
        reactor.listenUDP(MESSAGE, the_messenger)

    echo_request = task.LoopingCall(the_messenger.echo_request)

    if configuration.has_key('echo_time'):
        echo_request.start(configuration['echo_time'])
    else:
        print "run(): No echo_time specified in config, using default of", ECHO_TIME
        echo_request.start(ECHO_TIME)

    reactor.run()

    print "run(): Ending..."

    return

def handler(signal, frame):

    global running
    running = 0

    reactor.stop()

    return

def is_end():

    if not running:
        reactor.stop()

    return

def parse_config(filename):

    parsed = {'error': 1}

    if (exists(filename)):
        dom = parse(filename)
    else:
        print "parse_config():", filename, "doesn't exist"
        return parsed

    if (dom.childNodes[0].nodeName == 'server_config'):
        for node in dom.childNodes[0].childNodes:

            if (node.nodeName == 'name' and len(node.childNodes)):
                parsed['name'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'password' and len(node.childNodes)):
                parsed['password'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'broadcast' and len(node.childNodes)):
                parsed['broadcast'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'message' and len(node.childNodes)):
                parsed['message'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'echo_time' and len(node.childNodes)):
                parsed['echo_time'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'echo_number' and len(node.childNodes)):
                parsed['echo_number'] = int(node.childNodes[0].nodeValue)

    parsed['error'] = 0

    return parsed

class Broadcast(DatagramProtocol):

    def startProtocol(self):

        self.transport.joinGroup(MULTICAST)

    def datagramReceived(self, data, address):

        global parsed_config

        if data[0:len(SERVERREQUEST)] == SERVERREQUEST:

            new_message = ''.join([SERVEROFFER, " ",
                                   str(parsed_config['message']),
                                   " ", parsed_config['name']])

            destination = (address[0], int(data[len(SERVERREQUEST) + 1:]))

            the_messenger.transport.write(new_message, destination)

            print "Broadcast.datagramReceived(): Received SERVERREQUEST, responded with SERVEROFFER"

        return

class Messenger(DatagramProtocol):

    def datagramReceived(self, data, (host, port)):

        global members
        global parsed_config

        if data[:len(WANTIN)] == WANTIN:

            if not members.has_key(data[len(WANTIN) + 1:]):
                print "Messenger.datagramReceived(): WANTIN received, responding with YOUREIN"

                response = ''.join([SOMEONEJOINED, " ", data[len(WANTIN) + 1:]])

                for member in members:
                    self.transport.write(response, members[member])

                members[data[len(WANTIN) + 1:]] = (host, port)
                response = YOUREIN
                self.transport.write(response, (host, port))

            else:
                if (host, port) in members.values():
                 print "Messenger.datagramReceived(): WANTIN received from a current member, responding with YOUREIN"
	         response = YOUREIN
                 self.transport.write(response, (host, port))

                else:
                  response = NAMEDENIED
                  self.transport.write(response, (host, port))

        if data[:len(IMHERE)] == IMHERE:

            if (host, port) in members.values():

                # print "Messenger.datagramReceived(): IMHERE received"

                for name in members:
                    if members[name] == (host, port):
                        if name in self.echo_members:
                            while self.echo_members.count(name):
                                self.echo_members.remove(name)

        if data[:len(YOUTHERE)] == YOUTHERE:

            #print "Messenger.datagramReceived(): YOUTHERE received, responding with IMHERE"

            self.transport.write(IMHERE, (host, port))

        if data[:len(IMOUT)] == IMOUT:

            if (host, port) in members.values():
                print "Messenger.datagramReceived(): IMOUT received, notifying others"

                response = ''.join([SOMEONELEFT, " "])

                the_name = ''
                for name in members.keys():
                    if members[name] == (host, port):
                        the_name = name

                self.ejectMember(the_name)

            else:
                print "Messenger.datagramReceived(): IMOUT received, but sender not present in listing"

        if data[:len(GETLIST)] == GETLIST:
            print "Messenger.datagramReceived(): GETLIST received, responding with LIST"

            response = ''.join([LIST, ' '])

            for iname in members:
                response = ''.join([response, iname, ' '])

            self.transport.write(response, (host, port))

        if data[:len(SERVERKILL)] == SERVERKILL:

            if data[len(SERVERKILL) + 1:] == parsed_config['password']:
                print "Messenger.datagramReceived(): SERVERKILL received, dying now."
                reactor.stop()

        if data[:len(LETTER)] == LETTER:

            responses = []

            origin = ''

            for name in members:
                if members[name] == (host, port):
                    origin = name

            if origin == '':
                print "Messenger.datagramReceived(): LETTER received, but originator not in listing"
            else:
                print "Messenger.datagramReceived(): LETTER received, forwarding it on..."

                response = [''.join([LETTER, ':', origin, ':']), []]
                message = ''

                if data[len(LETTER):len(LETTER) + 2] == '::':

                    message = data[data.find("::") + 2:]
                    response[0] = ''.join([response[0], message])

                    for name in members:
                        if origin != name:

                            responses.append(response)
                            responses[-1][1] = members[name]
                else:

                    message = data[the_data.find(':', data.find(':') + 1) + 1:]
                    response[0] = ''.join([response[0], messsage])

                    string_parts = data[data.find(':') + 1:
                                        data.find(':', data.find(':') + 1)].split()

                    for dest in string_parts:

                        if members.has_key(dest):

                            responses.append(response)
                            responses[-1][1] = members[dest]

                for response_i in responses:

                    self.transport.write(response_i[0], response_i[1])

        return

    def ejectMember(self, name):

        reactor.callFromThread(self.ejectThreadMember, name)

        return

    def ejectThreadMember(self, name):
        if name in members.keys():
            response = YOUROUT
            self.transport.write(response, members[name])

            del members[name]

            response = ''.join([SOMEONELEFT, " ", name])

            for member in members:

                self.transport.write(response, members[member])
                print "notify debug", members[member], response

        return

    def echo_request(self):

        global members
        global parsed_config
        global ECHO_COUNT

        ejecters = []

        if len(members):
            #print "Messenger.echo_request(): Sending YOUTHERE messages..."
            pass

        for member in members:

            if self.echo_members.count(member) > ECHO_COUNT:
                ejecters.append(member)

            else:
                self.transport.write(STILLIN, members[member])
                self.echo_members.append(member)

        for person in ejecters:

            self.ejectMember(person)
            print "Messenger.echo_request(): Member ejected for suspicion of communism, name:", person

            temp_members = []

            for echo_request_item in self.echo_members:
                if person != echo_request_item:
                    temp_members.append(echo_request_item)

            self.echo_members = temp_members

        return

if __name__ == '__main__':
    main()
