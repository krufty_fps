#!/usr/bin/python

# PyOpenGL + Pygame

# various constants

CONFIG = 'game_config.xml'
PROTOCOL_FILE = 'protocol.py'

WINDOW_SIZE = (640, 480)
WINDOW_CAPTION = "Testing stuff"

BROADCAST = 50030
BROADCAST_BIND = 50031

MESSAGE = 40030
BUFSIZE = 120
ECHO_TIME = 5
TIMEOUT = .1
BROADCAST = ''
MULTICAST = '234.0.0.1'
SERVER_TIMEOUT = 0

protocol_file = open(PROTOCOL_FILE)
exec(protocol_file)
protocol_file.close()

the_functions = {}
in_script = 0

import os, sys
from os import *
from os.path import *
sys.path.insert(0, 'pygame_gui')

import threading
from socket import *
from xml.dom.minidom import parse, parseString
from threading import Lock
from signal import *
from zipfile import *

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.internet import task

import pygame
from pygame import *

import lamina

import OpenGL
import OpenGL.GL
from OpenGL.GL import *
import OpenGL.GLU
from OpenGL.GLU import *

from DerGUI import GUISystem

the_event_receiver = 0

global the_engine
global the_client

class Engine:

    def __init__(self, config):

        self.config = config
        self.running = 1

        global the_engine
        the_engine = self

        video_flags = OPENGL|DOUBLEBUF

        pygame.init()
        self.display = pygame.display.set_mode(config['window_size'], video_flags)
        self.resize((config['window_size']))

        glShadeModel(GL_SMOOTH)
        glClearColor(self.config['background_color'][0],
                     self.config['background_color'][1],
                     self.config['background_color'][2],
                     self.config['background_color'][3])

        glClearDepth(1.0)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

        pygame.display.set_caption(config['window_title'])

        self.gui_screen = lamina.LaminaScreenSurface()

        self.gui_system = GUISystem('widgets.xml', 'theme_two.zip')
        self.change = 0

        self.files = {}
        self.events = EventReceiver()

        # self.files is a map of filename to file

        # The file has this:
        #  {parent, [children], {object_defs},
        #  {menu_defs}, {object_instances}, {menu_instances}}

        self.zipfiles = []

        for file in listdir(self.config['pack_dir']):
            if (is_zipfile(os.path.join(self.config['pack_dir'], file))):
                self.zipfiles.append(ZipFile(os.path.join(self.config['pack_dir'], file)))

        self.running = self.addFile(self.config['init'], '')

        if not self.running:
            print "Engine.__init__(): Failed adding initial file"

        return
    
    def run(self):

        global the_client

        self.gui_screen.refresh()

        while self.running:

            self.process_events()

            self.draw()

            pygame.display.flip()

        return

    def killGame(self):

        reactor.callFromThread(self.killThreadGame)

        return

    def killThreadGame(self):

        reactor.stop()

        return

    def process_events(self):

        change = 0

        for e in pygame.event.get():
            if e.type == QUIT:
                self.running = 0
                print "Should be quitting..."

                self.killGame()

            self.change = self.gui_system.event(e)

            new_event = self.convertEvent(e)
            self.events.addEvent(new_event)

        iterating = 1
        while iterating:

            if self.events.size():
                new_event = self.events.getEvent()
            else:
                iterating = 0

            if not iterating:
                return

            if iterating:
                for file in self.files.keys():

                    for name in self.files[file]['object_instances'].keys():

                        self.files[file]['object_instances'][name].event(new_event[0], new_event[1])

        return

    def convertEvent(self, e):

        data = {}

        if e.type == QUIT: 
            data['none'] = ''

        if e.type == ACTIVEEVENT: 
            data['gain'] = e.gain 
            data['state'] = e.state

        if e.type == KEYDOWN: 
            data['unicode'] = e.unicode 
            data['key'] = e.key 
            data['mod'] = e.mod

        if e.type == KEYUP: 
            data['key'] = e.key 
            data['mod'] = e.mod

        if e.type == MOUSEMOTION:	 
            data['pos'] = e.pos 
            data['rel'] = e.rel 
            data['buttons'] = e.buttons

        if e.type == MOUSEBUTTONUP: 
            data['pos'] = e.pos 
            data['button'] = e.button

        if e.type == MOUSEBUTTONDOWN: 
            data['pos'] = e.pos 
            data['button'] = e.button

        if e.type == JOYAXISMOTION: 
            data['joy'] = e.joy 
            data['axis'] = e.axis 
            data['value'] = e.value

        if e.type == JOYBALLMOTION: 
            data['joy'] = e.joy 
            data['ball'] = e.ball 
            data['rel'] = e.rel

        if e.type == JOYHATMOTION: 
            data['joy'] = e.joy 
            data['hat'] = e.hat 
            data['value'] = e.value

        if e.type == JOYBUTTONUP: 
            data['joy'] = e.joy 
            data['button'] = e.button

        if e.type == JOYBUTTONDOWN: 
            data['joy'] = e.joy 
            data['button'] = e.button

        if e.type == VIDEORESIZE: 
            data['size'] = e.size 
            data['w'] = e.w 
            data['h'] = e.h

        if e.type == VIDEOEXPOSE: 
            data['none'] = ''

        if e.type == USEREVENT: 
            data['code'] = e.code

        type = ''

        if e.type == QUIT: type = "QUIT"
        if e.type == ACTIVEEVENT: type = "ACTIVEEVENT"
        if e.type == KEYDOWN: type = "KEYDOWN"
        if e.type == KEYUP: type = "KEYUP"
        if e.type == MOUSEMOTION    : type = "MOUSEMOTION"
        if e.type == MOUSEBUTTONUP: type = "MOUSEBUTTONUP"
        if e.type == MOUSEBUTTONDOWN: type = "MOUSEBUTTONDOWN"
        if e.type == JOYAXISMOTION: type = "JOYAXISMOTION"
        if e.type == JOYBALLMOTION: type = "JOYBALLMOTION"
        if e.type == JOYHATMOTION: type = "JOYHATMOTION"
        if e.type == JOYBUTTONUP: type = "JOYBUTTONUP"
        if e.type == JOYBUTTONDOWN: type = "JOYBUTTONDOWN"
        if e.type == VIDEORESIZE: type = "VIDEORESIZE"
        if e.type == VIDEOEXPOSE: type = "VIDEOEXPOSE"
        if e.type == USEREVENT: type = "USEREVENT"

        return [type, data]

    def draw(self):

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()

        # draw stuff

        glLoadIdentity()

        if self.change:
            self.gui_system.draw(self.gui_screen.surf)
            self.gui_screen.refresh()
        self.gui_screen.display()

    def resize(self, (width, height)):
        if height==0:
            height=1
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45, 1.0*width/height, 0.1, 100.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        return

    def addFile(self, filename, parent):

      # Because I have alot of nesting in this function,
      # I am using 2 space formatting

      for zipfile in self.zipfiles:

        for file in zipfile.namelist():

          if (file == filename):
            dom = parseString(zipfile.read(file))

            object_instances_d = []
            menu_instances = []

            if dom.childNodes[0].nodeName == 'game':
              self.files[filename] = {
                  'object_defs': {},
                  'children': [],
                  'parent': '',
                  'object_instances': {},
                  'menu_instances': {},
                  'menu_defs': {}}

              for node in dom.childNodes[0].childNodes:

                if (node.nodeName == 'def'):

                  for sub_node in node.childNodes:

                    if (sub_node.nodeName == 'object'):

                      temp_object_def = {}

                      for suber_node in sub_node.childNodes:

                        if (suber_node.nodeName == 'name'):
                          if len(suber_node.childNodes):
                            temp_object_def['name'] = suber_node.childNodes[0].nodeValue

                        if (suber_node.nodeName == 'script'):
                          if len(suber_node.childNodes):
                            temp_object_def['script'] = suber_node.childNodes[0].nodeValue

                        if (suber_node.nodeName == 'tangible'):
                          if len(suber_node.childNodes):
                            temp_object_def['tangible'] = suber_node.childNodes[0].nodeValue

                        if (suber_node.nodeName == 'type'):
                          if len(suber_node.childNodes):
                            temp_object_def['type'] = suber_node.childNodes[0].nodeValue

                      self.files[filename]['object_defs'][temp_object_def['name']] = temp_object_def
                      temp_object_def = {}

                    if (sub_node.nodeName == 'menu'):

                      temp_menu_def = {}
                      temp_menu_def['elements'] = {}

                      temp_element_set = {}

                      for suber_node in sub_node.childNodes:

                        if (suber_node.nodeName == 'name'):
                          temp_menu_def['name'] = suber_node.childNodes[0].nodeValue

                        if (suber_node.nodeName == 'elements'):

                          for suberer_node in suber_node.childNodes:

                            if (suberer_node.nodeType != 3 and suberer_node.nodeType != 8):
                              if (suberer_node.hasAttribute('name')):
                                temp_element_set['name'] = suberer_node.getAttribute('name')

                              if (suberer_node.hasAttribute('x')):
                                temp_element_set['x'] = int(suberer_node.getAttribute('x'))

                              if (suberer_node.hasAttribute('y')):
                                temp_element_set['y'] = int(suberer_node.getAttribute('y'))

                              if (suberer_node.hasAttribute('width')):
                                temp_element_set['width'] = int(suberer_node.getAttribute('width'))

                              if (suberer_node.hasAttribute('height')):
                                temp_element_set['height'] = int(suberer_node.getAttribute('height'))

                              if (suberer_node.hasAttribute('parent')):
                                temp_element_set['parent'] = suberer_node.getAttribute('parent')

                              if (suberer_node.hasAttribute('target')):
                                temp_element_set['target'] = suberer_node.getAttribute('target')

                              if (suberer_node.hasAttribute('text')):
                                temp_element_set['text'] = suberer_node.getAttribute('text')

                              temp_element_set['type'] = suberer_node.nodeName
                              temp_menu_def['elements'][temp_element_set['name']] = temp_element_set
                              temp_element_set = {}

                      self.files[filename]['menu_defs'][temp_menu_def['name']] = temp_menu_def

                      temp_menu_def = {}
                      temp_element_set = {}

                if (node.nodeName == 'instance'):

                  for sub_node in node.childNodes:

                    if (sub_node.nodeName == 'object'):
                      object_instances_d.append(sub_node.childNodes[0].nodeValue)

                    if (sub_node.nodeName == 'menu'):
                      menu_instances.append(sub_node.childNodes[0].nodeValue)

            if (parent != ''):
              self.files[parent]['children'].append(filename)
            self.files[filename]['parent'] = parent

            # create instances

            for menuname, menu_data in self.files[filename]['menu_defs'].iteritems():
              self.files[filename]['menu_instances'][menuname] = Menu(menu_def=menu_data)

            for menuname in menu_instances:
              if (self.files[filename]['menu_instances'].has_key(menuname)):
                self.files[filename]['menu_instances'][menuname].show()

              for file in self.files.keys():
                if self.files[file]['menu_instances'].has_key(menuname):
                  self.files[file]['menu_instances'][menuname].show()

            for objectname in object_instances_d:
              self.addObject(objectname)

            return 1
      return 0
    # ending 2-space formatting

    def removeFile(self, filename):

        if (self.files.has_key(filename)):

            for child in self.files[filename]['children']:
                self.removeFile(child)

            del self.files[filename]['children']

            parent = self.files[filename]['parent']

            self.files[parent]['children'].pop(self.files[parent]['children'].index(filename))

            del self.files[filename]

        return

    def getVisibleMenus(self):

        menus = {}

        for file, contents in self.files.iteritems():
            for name, menu in contents['menu_instances'].iteritems():
                if not menu.hidden:
                    menus[name] = menu

        return menus

    def hideAllMenus(self):

        for file, contents in self.files.iteritems():
            for name, menu in contents['menu_instances'].iteritems():
                menu.hide()

        return

    def showMenu(self, menu_name):

        for file, contents in self.files.iteritems():
            for name, menu in contents['menu_instances'].iteritems():
                if contents['menu_instances'].has_key(menu_name):
                    contents['menu_instances'][menu_name].show()

        return

    def hideMenu(self, menu_name):

        for file, contents in self.files:
            for name, menu in contents['menu_instances'].iteritems():
                if contents['menu_instances'].has_key(menu_name):
                    contents['menu_instances'][menu_name].hide()

        return

    def setMenu(self, menu_name):

        self.hideAllMenus()
        self.showMenu(menu_name)

        return

    def addObject(self, objectname):

        for file in self.files.keys():
            if self.files[file]['object_defs'].has_key(objectname):
                self.files[file]['object_instances'][objectname] = GameObject(
                    self.files[file]['object_defs'][objectname])

        return

    def removeObject(self, objectname):

        for file in self.files.keys():
            if self.files[file]['object_defs'].has_key(objectname):
                del self.files[file]['object_instances'][objectname]

        return

    def addListItem(self, menuname, item, data, dvalue):

        print "Engine.addListItem(): Not implemented yet."

        return

    def getImage(self, filename):

        the_buf = pygame.image.load(self.getStringIOFile(filename))

        return the_buf

    def getStringIOFile(self, filename):

        for zip_file in self.zipfiles:
            for file in zip_file.namelist():

                if (filename == file):

                    the_string_io = StringIO.StringIO()
                    print >>the_string_io, zip_file.read(file)
                    the_string_io.seek(0)

                    return the_string_io
        return ''

    def getStringData(self, filename):
        for zip_file in self.zipfiles:
            for file in zip_file.namelist():

                if (filename == file):
                    return zip_file.read(file)
        return ''

class Menu:

    def __init__(self, menu_def):

        self.menu_def = menu_def
        self.widgets = {}

        self.gui_system = the_engine.gui_system

        for name, element in menu_def['elements'].iteritems():

            multiplier_x = float(the_engine.config['window_size'][0]) / 100
            multiplier_y = float(the_engine.config['window_size'][1]) / 100.00

            dx = float(element['x']) * multiplier_x
            dy = float(element['y']) * multiplier_y

            dwidth = float(element['width']) * multiplier_x
            dheight = float(element['height']) * multiplier_y

            params = {'x': dx, 'y': dy, 'width': dwidth, 'height': dheight }

            if element['type'] == 'image':
                print "Widget.__init__(): 'image' widget type not implemented yet."

            if element['type'] == 'label':
                params['text'] = element['text']
                self.widgets[name] = self.gui_system.makeWidget('label', params)

            if element['type'] == 'button':
                params['text'] = element['text']
                self.widgets[name] = self.gui_system.makeWidget('button', params)
                self.widgets[name].connect('BUTTON_CLICKED', self.clicked, {'name': name})

            if element['type'] == 'listbox':
                print "Widget.__init__(): 'listbox' widget type not implemented yet."

            if element['type'] == 'textbox':
                params['type'] == element['text']
                self.widgets[name] = self.gui_system.makeWidget('textbox', params)
                self.widgets[name].connect('KEYDOWN', self.key_pressed, {'name': name})

        self.hidden = 0

        return

    def show(self):

        for widget in self.widgets:
            self.widgets[widget].show()

        self.hidden = 0

        return

    def hide(self):

        for widget in self.widgets:
            self.widgets[widget].hide()

        self.hidden = 1

        return

    def clicked(self, params, more_params):

        #if self.menu_def['elements'].has_key()
        # to be continued

        return

class GameObject:

    def __init__(self, dmold):

        self.mold = dmold
        global the_functions
        global in_script

        if self.mold['script'] != '':

            in_script = 1

            the_file = the_engine.getStringData(self.mold['script'])
            if the_file != '':
                exec(the_file)

            in_script = 0
            self.my_functions = the_functions

            the_functions = {}

    def event(self, event, data):

        if self.my_functions.has_key(event):

            self.my_functions[event](data)

        return

class Multicast(DatagramProtocol):

    def startProtocol(self):
        self.transport.joinGroup(MULTICAST)

    def datagramReceived(self, datagram, address):

        pass

class QueueLock:
    def __init__(self):

        self.items = []
        self.items_lock = Lock()

    def size(self):

        return len(self.items)

    def getItem(self):

        self.items_lock.acquire()
        the_item = self.items.pop(0)
        self.items_lock.release()

        return the_item

    def addItem(self, new_item):

        self.items_lock.acquire()
        self.items.append(new_item)
        self.items_lock.release()

        return

class EventReceiver(QueueLock):

    def getEvent(self):
        return self.getItem()
    def addEvent(self, event):

        self.addItem(event)
        return

class CommandLine(QueueLock):

    def getCommand(self):
        return self.getItem()
    def addCommand(self, command):
        self.addItem(command)
        return

class Client(DatagramProtocol):

    def datagramReceived(self, data, (host, port)):

        if data[:len(YOUTHERE)] == YOUTHERE:

#            print "Client.datagramReceived(): Recieved YOUTHERE, responding with IMHERE"
            self.transport.write(IMHERE, (host, port))

        if data[:len(SERVEROFFER)] == SERVEROFFER:

            split_strings = data.split()

            self.servers[split_strings[2]] = (host, int(split_strings[1]))

            event = [SERVEROFFER, {'server_name': split_strings[2],
                                   'server_address': self.servers[split_strings[2]]}]
            the_engine.events.addEvent(event)

            print "Client.datagramReceived(): Received SERVEROFFER"

        if data[:len(YOUREIN)] == YOUREIN:

            if (host, port) == self.requested_server:
                self.current_server = (host, port)
                self.requested_server = ()

                event = [YOUREIN]
                data = {'server': self.current_server}
                event.append(data)

                the_engine.events.addEvent(event)

                print "Client.datagramReceived(): Received YOUREIN, joined server"

                return

        if data[:len(LIST)] == LIST:

            print "Client.datagramReceived(): Received LIST"

            split_strings = data.split()

            self.members = []

            for string in split_strings:
                if string != LIST:
                    self.members.append(string)

            event = [LIST]
            data = {'names': self.members, 'server_address': (host, port)}
            event.append(data)

            the_engine.events.addEvent(event)

            return

        if data[:len(SOMEONEJOINED)] == SOMEONEJOINED:

            if (host, port) == self.current_server:

                print "Client.datagramReceived(): Received SOMEONEJOINED"

                left_member = ''

                for member in self.members:
                    if member == data[len(SOMEONEJOINED) + 1:]:
                        left_member = member

                if left_member == '':
                    self.members.append(data[len(SOMEONEJOINED) + 1:])

                    event = [SOMEONEJOINED]
                    data = {'name': data[len(SOMEONEJOINED) + 1:]}

                    event.append(data)

                    the_engine.events.addEvent(event)

            return

        if data[:len(SOMEONELEFT)] == SOMEONELEFT:

            if (host, port) == self.current_server:

                name = data[len(SOMEONELEFT) + 1:]

                if name in self.members:
                    print "Client.datagramReceived(): Received SOMEONELEFT"

                    self.members.remove(name)

                    event = [SOMEONELEFT]
                    data = {'name': name}

                    event.append(data)

                    the_engine.events.addEvent(event)

                else:
                    print "Client.datagramReceived(): Received SOMEONELEFT, but", name, "not present in roster"

                return

        if data[:len(YOUROUT)] == YOUROUT:

            if (host, port) == self.current_server:

                print "Client.datagramReceived(): Recieved YOUROUT"

                self.current_server = ()

                event = [YOUROUT]
                data = {}
                event.append(data)

                the_engine.events.addEvent(event)

                return

        if data[:len(LETTER)] == LETTER:

            if (host, port) == self.current_server:
                print "Client.datagramReceived(): Received LETTER"

                split_strings = data.split(':')

                message = data[data.find(':', len(LETTER) + 1) + 1:]
                message_origin = split_strings[1]

                event = [LETTER]
                data = {'message': message, 'origin': message_origin}
                event.append(data)

                the_engine.events.addEvent(event)

                return

        if data[:len(IMHERE)] == IMHERE:

            if (host, port) == self.current_server:
                print "Client.datagramReceived(): Received IMHERE from server"

                self.server_request = 0

        return

    def init(self, params):

        self.params = params

        self.current_server = ()
        self.requested_server = ()
        self.servers = {}

        self.inited = 1

        return

    def serverRequest(self):

        if self.current_server != ():

            if self.server_request > SERVER_TIMEOUT:
                the_engine.events.addEvent([SERVER_GONE, {}])
                self.current_server = ()

            else:
                self.server_request += 1
                self.transport.write(YOUTHERE, self.current_server)

        return

    def executeCommand(self, command, data):

        reactor.callFromThread(self.executeThreadedCommand, command, data)

        return

    def executeThreadedCommand(self, command, data):
        if command == SERVERKILL:

            if self.current_server != ():

                message = ''.join([SERVERKILL, ' '])

                if data.has_key('password'):
                    message = ''.join([message, data['password']])

                self.transport.write(message, self.current_server)

        if command == SERVERREQUEST:

            message = ''.join([SERVERREQUEST, ' ', str(self.params['message_port'])])

            the_multicast.transport.write(message, (MULTICAST, self.params['broadcast_port']))
#            self.transport.write(message, ('255.255.255.255', self.params['broadcast_port']))

        if command == GETLIST:

            message = GETLIST

            if data.has_key('server'):

                if self.servers.has_key(data['server']):
                    self.transport.write(message, self.servers[data['server']])

            else:

                if self.current_server != ():
                    self.transport.write(message, self.current_server)

        if command == IMOUT:

            if self.current_server != ():

                message = IMOUT

                self.transport.write(message, self.current_server)

                self.current_server = ()

        if command == LETTER:

            if self.current_server != ():

                message = ''.join([LETTER, ':'])

                for dest in data['destinations']:
                    if dest in self.members:
                        message = ''.join([message, ' ' , dest])

                message = ''.join([message, ':', data['message']])

                self.transport.write(message, self.current_server)

        if command == WANTIN:

            if data.has_key('server'):
                if self.servers.has_key(data['server']):

                    self.transport.write(''.join([WANTIN, ' ', self.params['name']]),
                                         self.servers[data['server']])
                    self.current_server = ()
                    self.requested_server = self.servers[data['server']]

        return

def main():

    if len(sys.argv) > 1:
        config = sys.argv[1]
    else:
        print "main(): No config specified, using", CONFIG
        config = CONFIG

    configuration = parse_config(config)

    if (configuration.has_key('error') and configuration['error']):
        print "main(): Error in parsing config."

    the_event_receiver = EventReceiver()

    global client_params
    client_params = {'broadcast_port':configuration['broadcast_port'],
                     'broadcast_bind':configuration['broadcast_bind'],
                     'message_port':configuration['message_port'],
                     'name':configuration['name'],
                     'echo_time': configuration['echo_time']}

    global the_engine
    global the_client

    the_engine = Engine(configuration)

    the_client = Client()
    the_client_thread = threading.Thread(target=run_net)

    the_client.server_request = 0

    the_client.init(client_params)

    the_client_thread.start()
    the_engine.run()

    the_engine.killGame()

    sys.exit()

    the_client_thread.join()

    return

def run_net():

    global client_params
    global the_multicast

    the_multicast = Multicast()
    config = client_params

    reactor.listenMulticast(config['broadcast_bind'], the_multicast)
    reactor.listenUDP(config['message_port'], the_client)

    echo_request = task.LoopingCall(the_client.serverRequest)

    if client_params['echo_time'] != None:
        echo_request.start(client_params['echo_time'])
    else:
        print "run_net(): No ECHO_TIME specified, using default of", ECHO_TIME
        echo_request.start(ECHO_TIME)

    reactor.run(installSignalHandlers=0)

    return

def parse_config(filename):
    results = {'error': 1}

    if (exists(filename)):
        dom = parse(filename)
    else:
        print "parse_config():", filename, "doesn't exist."
        return results

    results = {'window_title': None,
               'log': None,
               'name': None,
               'font': None,
               'init': None,
               'message_port': None,
               'broadcast_port': None,
               'broadcast_bind': None,
               'fullscreen': None,
               'background_color': None,
               'window_size': None,
               'gravity': None,
               'step_size': None,
               'pack_dir': None,
               'echo_time': None}

    if (dom.childNodes[0].nodeName == 'config'):
        for node in dom.childNodes[0].childNodes:

            if (node.nodeName == 'window_title'):
                results['window_title'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'log'):
                results['log'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'name'):
                results['name'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'font'):
                results['font'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'init'):
                results['init'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'message_port'):
                results['message_port'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'broadcast_port'):
                results['broadcast_port'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'broadcast_bind'):
                results['broadcast_bind'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'fullscreen'):
                results['fullscreen'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'background_color'):

                string_parts = node.childNodes[0].nodeValue.split()
                results['background_color'] = [float(string_parts[0]), float(string_parts[1])]
                results['background_color'].append(float(string_parts[2]))
                results['background_color'].append(float(string_parts[3]))

            if (node.nodeName == 'window_size'):

                string_parts = node.childNodes[0].nodeValue.split()
                results['window_size'] = ((int(string_parts[0]), int(string_parts[1])))

            if (node.nodeName == 'gravity'):

                string_parts = node.childNodes[0].nodeValue.split()
                results['gravity'] = [float(string_parts[0]), float(string_parts[1])]
                results['gravity'].append(float(string_parts[2]))

            if (node.nodeName == 'step_size'):
                results['step_size'] = float(node.childNodes[0].nodeValue)

            if (node.nodeName == 'pack_dir'):
                results['pack_dir'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'echo_time'):
                results['echo_time'] = int(node.childNodes[0].nodeValue)

    results['error'] = 0

    return results

if __name__ == '__main__':
    main()
