# lesson04 in pygame with my gui

import sys
sys.path.insert( 0, '..' )

from OpenGL.GL import *
from OpenGL.GLU import *

import pygame
from pygame import *

import lamina

from DerGUI import GUISystem

rtri = rquad = 0.0

triOn = quadOn = True

def resize((width, height)):
    if height==0:
        height=1
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, 1.0*width/height, 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def init():
    glShadeModel(GL_SMOOTH)
    glClearColor(0.0, 0.5, 0.0, 0.0)
    glClearDepth(1.0)
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LEQUAL)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

def draw():

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    glTranslatef(-1.5, 0.0, -6.0)

    # draw triangle
    global rtri
    glRotatef(rtri, 0.0, 1.0, 0.0)

    glBegin(GL_TRIANGLES)
    glColor3f(1.0, 0.0, 0.0)
    glVertex3f(0.0, 1.0, 0.0)
    glColor3f(0.0, 1.0, 0.0)
    glVertex3f(-1.0, -1.0, 0)
    glColor3f(0.0, 0.0, 1.0)
    glVertex3f(1.0, -1.0, 0)
    glEnd()

    # draw quad
    glLoadIdentity()
    glTranslatef(1.5, 0.0, -6.0)
    global rquad
    glRotatef(rquad, 1.0, 0.0, 0.0)

    glColor3f(0.5, 0.5, 1.0)
    glBegin(GL_QUADS)
    glVertex3f(-1.0, 1.0, 0)
    glVertex3f(1.0, 1.0, 0)
    glVertex3f(1.0, -1.0, 0)
    glVertex3f(-1.0, -1.0, 0)
    glEnd()

    glLoadIdentity()
    global the_surf
    global gui_system
    global gui_screen

    if change:
        gui_system.draw(the_surf)
        gui_screen.refresh()
    gui_screen.display()

def main():

    global rtri, rquad, gui_screen
    video_flags = OPENGL|DOUBLEBUF

    pygame.init()
    pygame.display.set_mode((640,480), video_flags)

    resize((640,480))
    init()

    global gui_screen
    gui_screen = lamina.LaminaScreenSurface()
    global the_surf
    the_surf = gui_screen.surf

    # func to toggle triangle spin
    def triTog(l, o):
        global triOn
        triOn = not triOn
    # func to toggle quad spin
    def quadTog(l, d):
        global quadOn
        quadOn = not quadOn

    global gui_system
    gui_system = GUISystem('widgets.xml', 'theme_two.zip')

    global widgets
    widgets = {}

    params_a = {'x': 29, 'y': 13, 'width': 200,
                'height': 50, 'text':'Lamina Demo: PGU'}
    widgets['title'] = gui_system.makeWidget('label', params_a)

    params_b =  {'x': 80, 'y': 400, 'width': 200,
                'height': 50, 'text':'Stop/Start Triangle'}
    widgets['button_1'] = gui_system.makeWidget('button', params_b)
    widgets['button_1'].connect('BUTTON_CLICKED', triTog)

    params_c =  {'x': 380, 'y': 400, 'width': 200,
                'height': 50, 'text':'Stop/Start Quad'}
    widgets['button_2'] = gui_system.makeWidget('button', params_c)
    widgets['button_2'].connect('BUTTON_CLICKED', quadTog)

    quit = 0

    global change
    change = 0

    gui_system.draw(the_surf)
    gui_screen.refresh()

    while not quit:
        getting_events = 1
        while getting_events:

            the_event = pygame.event.poll()
            if the_event.type == pygame.QUIT:
                quit = 1
            if the_event.type == pygame.NOEVENT:
                getting_events = 0

            change = 0
            change = gui_system.event(the_event)

        draw()

        if triOn:
            rtri += 0.2
        if quadOn:
            rquad += 0.2

        pygame.display.flip()

if __name__ == '__main__': main()
#import profile
#profile.run('main()')
