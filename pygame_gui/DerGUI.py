#!/usr/bin/python

import os, sys
from os import *
from os.path import *

from xml.dom.minidom import parse, parseString

from zipfile import *

import pygame
from pygame import *

import StringIO
from string import *

import copy

import time

import array

FONT_NAME = 'serif'
FONT_SIZE = 20
FONT_COLOR = (255, 255, 255)

BLINK_DELAY = 3.0
BLINK_SPEED = .05

class GUISystem:

    def __init__(self, gui_def, theme):

        assert is_zipfile(theme)
        assert exists(gui_def)

        self.zipfile = ZipFile(theme)

        self.definitions = self.parse_gui_def(gui_def)

        pygame.init()

        assert pygame.font.get_init()

        self.the_font = pygame.font.SysFont(FONT_NAME, FONT_SIZE)

        self.images = {}

        assert self.getImages()

        self.widgets = []

        self.keys = {}
        self.last_time = -1

        self.hit = 0

    def parse_gui_def(self, gui_def):
        results = {}

        dom = parse(gui_def)

        if dom.childNodes[0].nodeName == 'widgets':

            widgets = {}

            for node in dom.childNodes[0].childNodes:

                image_list = []

                if node.nodeName == 'images':

                    for sub_node in node.childNodes:
                        if sub_node.nodeName == 'image':

                            image = {'filename': '', name: ''}

                            for sub_sub_node in sub_node.childNodes:

                                if sub_sub_node.nodeName == 'filename':
                                  image['filename'] = sub_sub_node.childNodes[0].nodeValue
                                if sub_sub_node.nodeName == 'name':
                                  image['name'] = sub_sub_node.childNodes[0].nodeValue

                            if 'posix' in image:
                                del image['posix']
                            image_list.append(image)

                    results['image_list'] = image_list

                if node.nodeName == 'widget':

                    widget = {}

                    widget['parameters'] = {'direct': 0}
                    widget['parts'] = {}
                    widget['behaviors'] = {}

                    for sub_node in node.childNodes:
                        if sub_node.nodeName == 'name' and len(sub_node.childNodes):
                            widget['name'] = sub_node.childNodes[0].nodeValue

                        if sub_node.nodeName == 'parameter':

                            parameter = {}
                            parameter['name'] = sub_node.attributes['name'].value

                            if sub_node.attributes.has_key('type'):
                                if sub_node.attributes['type'].value == 'int':
                                    try:
                                        widget['parameters'][parameter['name']] = int(sub_node.attributes['default'].value)
                                    except:
                                        widget['parameters'][parameter['name']] = sub_node.attributes['default'].value

                            else:
                                widget['parameters'][parameter['name']] = sub_node.attributes['default'].value

                        if sub_node.nodeName == 'part':

                          part_name = sub_node.getElementsByTagName('name')[0].childNodes[0].nodeValue
                          widget['parts'][part_name] = {}

                          for sub_sub_node in sub_node.childNodes:

                            if sub_sub_node.nodeName == 'type':
                                widget['parts'][part_name]['type'] = sub_sub_node.childNodes[0].nodeValue.encode()
                                if widget['parts'][part_name]['type'].find(':direct') != -1:
                                    widget['parameters']['direct'] = 1
                                if widget['parts'][part_name]['type'] == 'text':
                                  widget['parts'][part_name]['prerender'] = ''
                                  widget['parts'][part_name]['old_text'] = ''

                            if sub_sub_node.nodeName == 'source':
                              widget['parts'][part_name]['source'] = sub_sub_node.childNodes[0].nodeValue.encode()

                            if sub_sub_node.nodeName == 'position':
                              widget['parts'][part_name]['position'] = sub_sub_node.childNodes[0].nodeValue.encode()

                            if sub_sub_node.nodeName == 'size':
                              widget['parts'][part_name]['size'] = sub_sub_node.childNodes[0].nodeValue.encode()

                            if sub_sub_node.nodeName == 'zindex':
                              widget['parts'][part_name]['zindex'] = int(sub_sub_node.childNodes[0].nodeValue.encode())

                          if not widget['parts'][part_name].has_key('zindex'):
                            widget['parts'][part_name]['zindex'] = 1

                        if sub_node.nodeName == 'behavior':

                          event_type = sub_node.getElementsByTagName('on_event')[0].childNodes[0].nodeValue
                          the_do = sub_node.getElementsByTagName('do')[0].childNodes[0].nodeValue

                          widget['behaviors'][event_type] = the_do

                    widgets[widget['name']] = widget

            results['widgets'] = widgets

        return results

    def getImages(self):

        image_list = self.definitions['image_list']
        name_list = []

        name_list = self.zipfile.namelist()

        for image in image_list:

            if image['filename'] not in name_list:
                return 0

            else:
                the_string_io = StringIO.StringIO()
                print >>the_string_io, self.zipfile.read(image['filename'])
                the_string_io.seek(0)

                self.images[image['name']] = pygame.image.load(the_string_io).convert_alpha()

        return 1

    def event(self, the_event):

        if not len(self.widgets):
            return

        new_event = self.convertEvent(the_event)
        count = 0

        if new_event['type'] == 'NOEVENT':

            if self.last_time == -1:
                self.last_time = time.time()

            else:

                temp_time = time.time()

                for key in self.keys:

                    if not self.keys[key].has_key('first_time'):
                        if temp_time - BLINK_SPEED > self.last_time:
                            self.last_time = temp_time
                            self.focus.event({'type': 'KEYSTILLDOWN', 'data': self.keys[key]})
                            count += 1
                    else:
                        if temp_time - (BLINK_SPEED + BLINK_DELAY) > self.last_time:

                            self.last_time = temp_time
                            del self.keys[key]['first_time']
                            self.focus.event({'type': 'KEYSTILLDOWN', 'data': self.keys[key]})
                            count += 1

        if not new_event['type'].find('MOUSE') == -1:

            for widget in self.widgets:

                widget_rect = widget.getRect()
                if new_event['type'] != 'MOUSEBUTTONUP':
                    if widget_rect.collidepoint(new_event['data']['pos']):
                        if new_event['type'] != 'MOUSEMOTION' and widget.is_visible():
                            self.focus = widget
                            self.focus.event(new_event)
                            count += 1
                            self.hit = 1
                else:
                    if self.hit:
                        self.focus.event(new_event)
                        count += 1
                        self.hit = 0
        else:
            if new_event['type'] == 'KEYDOWN':
                self.keys[new_event['data']['key']] = new_event['data']
                self.keys[new_event['data']['key']]['first_time'] = 1

            if new_event['type'] == 'KEYUP' and self.keys.has_key(new_event['data']['key']):
                del self.keys[new_event['data']['key']]

            self.focus.event(new_event)

        return count

    def convertEvent(self, e):

        data = {}

        if e.type == QUIT: 
            data['none'] = ''

        if e.type == ACTIVEEVENT: 
            data['gain'] = e.gain 
            data['state'] = e.state

        if e.type == KEYDOWN: 
            data['unicode'] = e.unicode 
            data['key'] = e.key 
            data['mod'] = e.mod

        if e.type == KEYUP: 
            data['key'] = e.key 
            data['mod'] = e.mod

        if e.type == MOUSEMOTION:	 
            data['pos'] = e.pos 
            data['rel'] = e.rel 
            data['buttons'] = e.buttons

        if e.type == MOUSEBUTTONUP: 
            data['pos'] = e.pos 
            data['button'] = e.button

        if e.type == MOUSEBUTTONDOWN: 
            data['pos'] = e.pos 
            data['button'] = e.button

        if e.type == JOYAXISMOTION: 
            data['joy'] = e.joy 
            data['axis'] = e.axis 
            data['value'] = e.value

        if e.type == JOYBALLMOTION: 
            data['joy'] = e.joy 
            data['ball'] = e.ball 
            data['rel'] = e.rel

        if e.type == JOYHATMOTION: 
            data['joy'] = e.joy 
            data['hat'] = e.hat 
            data['value'] = e.value

        if e.type == JOYBUTTONUP: 
            data['joy'] = e.joy 
            data['button'] = e.button

        if e.type == JOYBUTTONDOWN: 
            data['joy'] = e.joy 
            data['button'] = e.button

        if e.type == VIDEORESIZE: 
            data['size'] = e.size 
            data['w'] = e.w 
            data['h'] = e.h

        if e.type == VIDEOEXPOSE: 
            data['none'] = ''

        if e.type == USEREVENT: 
            data['code'] = e.code

        type = ''

        if e.type == QUIT: type = "QUIT"
        if e.type == NOEVENT: type = "NOEVENT"
        if e.type == ACTIVEEVENT: type = "ACTIVEEVENT"
        if e.type == KEYDOWN: type = "KEYDOWN"
        if e.type == KEYUP: type = "KEYUP"
        if e.type == MOUSEMOTION    : type = "MOUSEMOTION"
        if e.type == MOUSEBUTTONUP: type = "MOUSEBUTTONUP"
        if e.type == MOUSEBUTTONDOWN: type = "MOUSEBUTTONDOWN"
        if e.type == JOYAXISMOTION: type = "JOYAXISMOTION"
        if e.type == JOYBALLMOTION: type = "JOYBALLMOTION"
        if e.type == JOYHATMOTION: type = "JOYHATMOTION"
        if e.type == JOYBUTTONUP: type = "JOYBUTTONUP"
        if e.type == JOYBUTTONDOWN: type = "JOYBUTTONDOWN"
        if e.type == VIDEORESIZE: type = "VIDEORESIZE"
        if e.type == VIDEOEXPOSE: type = "VIDEOEXPOSE"
        if e.type == USEREVENT: type = "USEREVENT"

        new_event = {}

        new_event['type'] = type
        new_event['data'] = data

        return new_event

    def draw(self, dest_surface):

        for widget in self.widgets:

            if widget.params['parameters']['visible']:
                widget.draw(dest_surface)

        return

    def makeWidget(self, widget_name, nparams):

        params = copy.deepcopy(nparams)

        if self.definitions['widgets'].has_key(widget_name):

            for param in self.definitions['widgets'][widget_name]['parameters']:
                if not params.has_key(param):
                    if self.definitions['widgets'][widget_name]['parameters'][param] == 'kill':
                        print "GUISystem.makeWidget() Not all required parameters were passed."
                        assert 0
                    else:
                        params[param] = self.definitions['widgets'][widget_name]['parameters'][param]

            new_params = copy.deepcopy(self.definitions['widgets'][widget_name])
            new_params['parameters'] = params

            the_widget = Widget(self, widget_name, new_params)

            self.widgets.append(the_widget)

            if len(self.widgets) == 1:
                self.focus = the_widget

        return the_widget

class Widget:

    def __init__(self, the_gui_system, widget_name, params):

        self.name = widget_name
        self.params = params
        self.the_gui_system = the_gui_system

        assert self.params['parameters'].has_key('x')
        assert self.params['parameters'].has_key('y')
        assert self.params['parameters'].has_key('width')
        assert self.params['parameters'].has_key('height')

        if not self.params['parameters'].has_key('visible'):
            self.params['parameters']['visible'] = 1

        self.surf = pygame.Surface((int(self.params['parameters']['width']), int(self.params['parameters']['height'])), SRCALPHA)
        self.surf.convert_alpha()

        self.callbacks = {}

        return

    def move(self, (new_x, new_y)):

        self.params['parameters']['x'], self.params['parameters']['y'] = new_x, new_y

        return

    def hide(self):

        self.params['parameters']['visible'] = 0

        return

    def show(self):

        self.params['parameters']['visible'] = 1

        return

    def is_visible(self):

        return self.params['parameters']['visible']

    def connect(self, event, function, params={}):

        self.callbacks[event] = {'function': function, 'params': params}

        return

    def getRect(self):

        coords = (self.params['parameters']['x'], self.params['parameters']['y'])
        dimensions = (self.params['parameters']['width'], self.params['parameters']['height'])

        return pygame.Rect(coords, dimensions)

    def event(self, the_event):

        if self.params['behaviors'].has_key(the_event['type']):

            the_value = self.params['behaviors'][the_event['type']]

            #try:
            exec(the_value)
            #except:
            #print "Widget.event(): Error running behavior."

        return

    def setPart(self, old_part, new_part):

        self.params['parts'][old_part]['source'] = new_part
        return

    def raiseEvent(self, the_event, parameters={}):

        if not self.callbacks.has_key(the_event):
            return

        the_function = self.callbacks[the_event]['function']

        #try:
        the_function(self.callbacks[the_event]['params'], parameters)
        #except:
        #    print "Widget.raiseEvent(): Error in callback."

        return

    def draw(self, dest_surface):

        code = ''

        for image in self.the_gui_system.images:
            code = ''.join([code, '\n', image, ' = self.the_gui_system.images["', image,'"]'])

        try:
            exec(code)
        except:
            assert 0

        width = self.params['parameters']['width']
        height = self.params['parameters']['height']

        zindex_list = []
        for part in self.params['parts']:
            if not self.params['parts'][part]['zindex'] in zindex_list:
                zindex_list.append(self.params['parts'][part]['zindex'])

                if not self.params['parts'][part].has_key('old_text'):
                    self.params['parts'][part]['old_text'] = ''

        zindex_list.sort()

        #self.surf.fill(pygame.color.Color('0x00000000'))

        for zindex in zindex_list:
            for part in self.params['parts']:

                cur_part = self.params['parts'][part]

                # here be dragons
                if cur_part['type'].find('text') != -1:
                    try:
                        exec(''.join(['if self.params["parts"][part]["old_text"] != self.params["parameters"][self.params["parts"][part]["source"]]:\n    ', part, ' = self.the_gui_system.the_font.render(self.params["parameters"][self.params["parts"][part]["source"]], True, FONT_COLOR).convert_alpha(self.surf)\n    the_source = ', part, '\n    self.params["parts"][part]["prerender"] = ', part, '\n    self.params["parts"][part]["old_text"] = self.params["parameters"][self.params["parts"][part]["source"]]\nelse:\n    the_source = self.params["parts"][part]["prerender"]\n', part, ' = the_source']))
                    except:
                        assert 0

                position = list(eval(cur_part['position']))
                size = list(eval(cur_part['size']))

                for i in size:
                    size[size.index(i)] = int(i)

                position = [position[0], position[1]]

                if cur_part['zindex'] == zindex:

                    if cur_part['type'] == 'image':
                        the_source = pygame.transform.scale(self.the_gui_system.images[cur_part['source']], size)
                        self.surf.blit(the_source, position)

                    if cur_part['type'] == 'text':
                        the_source.convert_alpha(dest_surface)
                        self.surf.blit(the_source, position)

                    if cur_part['type'] == 'text:direct':
                        position = [position[0] + self.params['parameters']['x'],
                                    position[1] + self.params['parameters']['y']]
                        dest_surface.blit(the_source, position)

                    if cur_part['type'] == 'widget':
                        pass # implement

        if self.params['parameters'].has_key('direct') and self.params['parameters']['direct']:
            pass
        else:
            dest_surface.blit(self.surf, (self.params['parameters']['x'], self.params['parameters']['y']))
        return

# end
