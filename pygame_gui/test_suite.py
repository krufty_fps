#!/usr/bin/python

# this tests my gui

from DerGUI import *

WINDOW_SIZE = (640, 480)

THEME_FILE = 'theme_two.zip'
WIDGET_FILE = 'widgets.xml'

def main():

    the_display = pygame_init()
    make_gui()

    run(the_display)

    return

def pygame_init():

    pygame.init()
    return pygame.display.set_mode(WINDOW_SIZE)

def make_gui():

    global gui_system
    gui_system = GUISystem(WIDGET_FILE, THEME_FILE)

    global widgets
    widgets = {}

    params_a = {'x': 100, 'y': 50, 'width': 150, 'height': 50, 'text': 'a label'}
    widgets['test_label'] = gui_system.makeWidget('label', params_a)

    params_b = {'x': 75, 'y': 150, 'width': 200, 'height': 50, 'text': 'Hide/Show Label'}
    widgets['button_toggle'] = gui_system.makeWidget('button', params_b)

    params_c = {'x': 100, 'y': 250, 'width': 200, 'height': 35, 'text': ''}
    widgets['textbox_test'] = gui_system.makeWidget('textbox', params_c)

    widgets['button_toggle'].connect('BUTTON_CLICKED', button_clicked)

    return

def button_clicked(params, more_params):

    global widgets
    if widgets['test_label'].params['parameters']['visible']:
        widgets['test_label'].hide()
    else:
        widgets['test_label'].show()

    return

def run(the_display):

    my_image = pygame.image.load('turkey.jpg').convert()

    global gui_system

    quit = 0
    getting_events = 1

    while not quit:

        while getting_events:
            the_event = pygame.event.poll()
            if the_event.type == pygame.QUIT:
                quit = 1
            if the_event.type == pygame.NOEVENT:
                getting_events = 0

            gui_system.event(the_event)

        getting_events = 1

        the_display.blit(my_image, (0, 0))
        gui_system.draw(the_display)
        pygame.display.flip()

    return

if __name__ == '__main__':
    main()

# end
