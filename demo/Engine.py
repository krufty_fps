#!/usr/bin/python

CONFIG = 'game_config.xml'
INIT = 'run.xml'
PACK_DIR = 'pack'
WINDOW_SIZE = (640, 480)
BACKGROUND_COLOR = (1, 0, 0, 0)
GUI_THEME = 'theme_two.zip'
WIDGETS_FILE = 'widgets.xml'
TEMP_FILE = "dtemp.3ds"

import os, sys
#from os import *
from os.path import *

import pygame
from pygame import *

import lamina

from DerGUI import GUISystem

from zipfile import *

import copy

from xml.dom.minidom import parse, parseString

from experimental import *
from ThreadQueue import ThreadQueue

from PyOSG import osg, osgUtil, osgDB

from Entity import *

def main():

    print "Engine.py class... more documentation to come"

class Engine:

    def __init__(self, config_file=CONFIG):

        assert exists(config_file)
        dom = parse(config_file)
        self.config = reduceDOM(dom)
        self.config = reduceConfig(self.config)

        # pygame init stuff

        window_size = WINDOW_SIZE

        if self.config.has_key('window_size'):
            try:
                window_size = eval(self.config['window_size'])
            except:
                print "Engine.__init__(): Error in eval() using default window size of", WINDOW_SIZE

        video_flags = OPENGL|DOUBLEBUF
        self.display = pygame.display.set_mode(window_size, video_flags)

        # PyOSG init stuff

        background_color = BACKGROUND_COLOR

        if self.config.has_key('background_color'):
            try:
                background_color = eval(self.config['background_color'])
            except:
                print "Engine.__init__(): Error in eval(), using default background color of", BACKGROUND_COLOR

        self.root = osg.Group()

        self.sceneView = osgUtil.SceneView()
        self.sceneView.setDefaults()
        self.sceneView.init()
        self.sceneView.setSceneData(self.root)
        self.sceneView.setClearColor(
          osg.Vec4(background_color[0], background_color[1],
                   background_color[2], background_color[3]))

        # more stuff

        if self.config.has_key('window_title'):
            self.window_title = self.config['window_title']
        else:
            self.window_title = WINDOW_CAPTION
            print "Engine.__init__(): No window title specified, using default of", WINDOW_CAPTION

        pygame.display.set_caption(self.window_title)

        # lamina and gui stuff

        gui_theme = GUI_THEME

        if self.config.has_key('gui_theme'):
            gui_theme = self.config['gui_theme']
        else:
            print "Engine.__init__(): No gui theme specified, using default of", GUI_THEME

        self.lamina_screen = lamina.LaminaScreenSurface()
        self.gui_system = GUISystem(WIDGETS_FILE, gui_theme)

        self.change = None

        # zipfile stuff

        self.filelist = {}
        self.zipfiles = []

        pack_dir = PACK_DIR

        if self.config.has_key('pack_dir'):
            pack_dir = self.config['pack_dir']
        else:
            print "Engine.__init__(): No pack directory specified, using default of", PACK_DIR

        for file in os.listdir(pack_dir):
            if is_zipfile(os.path.join(pack_dir, file)):
                self.zipfiles.append(ZipFile(os.path.join(pack_dir, file)))

                for item in self.zipfiles[-1].namelist():
                    self.filelist[item] = "self.zipfiles[" + str(len(self.zipfiles) - 1) + "].read(\"" + item + "\")"

        # file stuff

        self.files = {}

        self.menudefs = {}
        self.objectdefs = {}
        self.objects = []
        self.menus = {}

        self.running = 1

        # thread stuff

        self.event_queue = None

    def addFile(self, filename, parent=''):

        if self.filelist.has_key(filename):

            dom = parseString(eval(self.filelist[filename]))
            assert dom != None

            parsed = reduceDOM(dom)
            instances = []

            results = {
              'menu_def': {},
              'object_def': {},
              'menu_instance': {},
              'object_instance': [],
              'parent': None,
              'children': []}

            for item in parsed[1][0]:

                if item[0] == 'def':

                    for defx in item[1][0]:

                        if defx[0] == 'object':
                            obj = reduceObjectDef(defx)
                            results['object_def'][obj['name']] = obj
                            self.objectdefs[obj['name']] = obj

                        if defx[0] == 'menu':
                            menu = reduceMenuDef(defx)
                            results['menu_def'][menu[0]] = menu
                            self.menudefs[menu[0]] = menu

                if item[0] == 'instance':

                    for defx in item[1][0]:

                        inst = reduceInstance(defx)
                        instances.append(inst)

            # do instance stuff here
            for item in instances:
                if item[0] == 'object':
                    assert item[1]['contents'] in self.objectdefs.keys()

                    d = {}
                    for n in item[1].keys():
                        d[n] = item[1][n]

                    for n in self.objectdefs[item[1]['contents']].keys():
                        d[n] = self.objectdefs[item[1]['contents']][n]

                    nobject = Entity(self, d)
                    results['object_instance'].append(nobject)
                    self.objects.append(nobject)

            results['parent'] = parent
            if not parent == '':
                self.files[parent]['children'].append(filename)

            self.files[filename] = results

            return 1

        else:
            return 0

    def readZipfile(self, filename):
        assert filename in self.filelist.keys()
        return eval(self.filelist[filename])

    def readNodeFile(self, filename):

        node = None
        error = 0
        temp_file = None

        if exists(TEMP_FILE):
            os.rename(TEMP_FILE, TEMP_FILE + ".bak")
            print "Engine.readNodeFile(): Renaming existing file..."

        #try:
        temp_file = open(TEMP_FILE, 'wb')

        #except:
        #    print "Engine.readNodeFile(): Error opening temp file.", TEMP_FILE
        #    error = 1

        if error != 1:
            try:
                data = self.readZipfile(filename)
            except:
                temp_file.close()
                print "Engine.readNodeFile(): Error reading filename"
                error = 1

        if error != 1:
            temp_file.write(data)

            temp_file.close()

            node = osgDB.readNodeFile(TEMP_FILE)

        if exists(TEMP_FILE + ".bak"):
            os.rename(TEMP_FILE + ".bak", TEMP_FILE)
        else:
            os.remove(TEMP_FILE)

        return node

    def run(self):

        if self.event_queue == None:
            self.event_queue = ThreadQueue()

        self.lamina_screen.clear()
        self.gui_system.draw(self.lamina_screen.surf)
        self.lamina_screen.refresh()

        while self.running:

            self.processEvents()
            self.draw()
            pygame.display.flip()

        return

    def processEvents(self):

        for e in pygame.event.get():
            if e.type == QUIT:
                self.running = 0
                print "Engine.processEvents(): Should be quitting..."

            self.change = self.gui_system.event(e)

            new_event = convertEvent(e)
            self.event_queue.append(new_event)

        iterating = 1
        while iterating:

            if len(self.event_queue):
                new_event = self.event_queue.pop()
            else:
                return

            for obj in self.objects:
                obj.event(new_event[0], new_event[1])

        return

    def draw(self):

        self.sceneView.update()
        self.sceneView.cull()
        self.sceneView.draw()

        self.lamina_screen.display()

        return

if __name__ == '__main__': main()
