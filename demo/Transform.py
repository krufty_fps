#!/usr/bin/python

import OpenGL
from OpenGL import *
import OpenGL.GL
from OpenGL.GL import *

def main():
    print "Transformation...more documentation to come"

class Transformation:

    def __init__(self, (x, y, z)=[0, 0, 0], (rotx, roty, rotz)=[0, 0, 0]):

        self.pos = list([x, y, z])
        self.rot = [rotx, roty, rotz]

    def negate(self):

        self.pos[0] = -self.pos[0]
        self.pos[1] = -self.pos[1]
        self.pos[2] = -self.pos[2]

        self.rot[0] = -self.rot[0]
        self.rot[1] = -self.rot[1]
        self.rot[2] = -self.rot[2]

        return

    def transform(self):

        glTranslatef(self.pos[0], self.pos[1], self.pos[2])
        glRotatef(self.rot[0], 1, 0, 0)
        glRotatef(self.rot[1], 0, 1, 0)
        glRotatef(self.rot[2], 0, 0, 1)

        return

    def negate_transform(self):

        self.fix()

        self.negate()
        self.transform()
        self.negate()

        return

    def fix(self):

        self.pos = list(self.pos)
        self.rot = list(self.rot)

        return

if __name__ == '__main__': main()
