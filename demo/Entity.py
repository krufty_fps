#!/usr/bin/python
# this is the Entity class

from PyOSG import osg, osgUtil, osgDB

def main():
    print "Entity class...more documentation to come"

class Entity:

    def __init__(self, engine, specs):

        self.functions = {}

        if specs.has_key('script') and specs['script'] != []:
            #try:
                self.script = engine.readZipfile(specs['script'])
                exec(self.script)
            #except:
            #    print "Entity.__init__(): Couldn't find/exec script"

        assert specs.has_key('contents')
        self.name = specs['contents']

        self.engine = engine

        self.transform = osg.PositionAttitudeTransform()

        if specs.has_key('model') and specs['model'] != '':
            #try:
            self.model = self.engine.readNodeFile(specs['model'])
            #except:
            #    print "Entity.__init__(): Error loading node file"
            #    self.model = None

            if self.model:
                self.transform.addChild(self.model)
                self.engine.root.addChild(self.transform)

        if specs.has_key('pos'):

            specs_pos = eval(specs['pos'])
            pos = osg.Vec3d(specs_pos[0],
                            specs_pos[1],
                            specs_pos[2])
            self.transform.setPosition(pos)

        if specs.has_key('rot'):
            specs_rot = eval(specs['rot'])
            rot = osg.Quat(specs_rot[0], specs_rot[1],
                           specs_rot[2], 1)
            self.transform.setAttitude(rot)

    def draw(self, engine):

        return

    def event(self, event, data):

        if self.functions.has_key(event):
            #try:
            self.functions[event](self, data)
            #except:
            #    print "Entity.event(): Error running event callback"

        return

if __name__ == '__main__': main()
