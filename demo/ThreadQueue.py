#!/usr/bin/python

import threading
from threading import Lock

class ThreadQueue:

    def __init__(self):
        self.items = []
        self.items_lock = Lock()

    def append(self, new_item):
        self.items_lock.acquire()
        self.items.append(new_item)
        self.items_lock.release()

    def pop(self):
        self.items_lock.acquire()
        if len(self.items):
            the_item = self.items.pop(0)
        else:
            the_item = None
        self.items_lock.release()

        return the_item

    def __len__(self):
        return len(self.items)
