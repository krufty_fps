#!/usr/bin/python
# this has some dom related functions

import pygame
from pygame import *

import OpenGL
import OpenGL.GL
from OpenGL.GL import *
import OpenGL.GLU
from OpenGL.GLU import *

def reduceDOM(dom):

    results = {}

    cur = []

    def parseLevel(level):
        parsed_stuff = [[], {}]

        if level.hasAttributes():
            for i in range(len(level.attributes)):

                attrname = level.attributes.item(i).name.encode()

                parsed_stuff[1][attrname] = level.attributes.item(1).value.encode()

        if len(level.childNodes) == 1 and level.childNodes[0].nodeType == 3:

            parsed_stuff[0] = level.childNodes[0].nodeValue.encode()

        else:

            parsed_stuff[0] = []

            for node in level.childNodes:
                if node.nodeType == 1:

                    name, contents = parseLevel(node)
                    parsed_stuff[0].append([name, contents])

        return level.nodeName.encode(), parsed_stuff

    results = parseLevel(dom.childNodes[0])

    return results

def reduceConfig(config):

    results = {}

    for item in config[1][0]:
        results[item[0]] = item[1][0]

    return results

def reduceObjectDef(file):

    results = {}

    for item in file[1][0]:

        results[item[0]] = item[1][0]

    return results

def reduceMenuDef(file):

    name = None
    script = None
    elements = {}

    for item in file[1][0]:

        if item[0] == 'name':
            name = item[1][0]

        if item[0] == 'script':
            script = item[1][0]

        if item[0] != 'script' and item[0] != 'name':
            temp = item[1][1]
            temp['contents'] = item[1][0]
            temp['type'] = item[0]
            elements[temp['id']] = temp

    return name, script, elements

def reduceInstance(item):

    type = item[0]
    results = item[1][1]

    results['contents'] = item[1][0]

    return type, results

def convertEvent(e):
    data = {}

    if e.type == QUIT: 
        data['none'] = ''

    if e.type == ACTIVEEVENT: 
        data['gain'] = e.gain 
        data['state'] = e.state

    if e.type == KEYDOWN: 
        data['unicode'] = e.unicode 
        data['key'] = e.key 
        data['mod'] = e.mod

    if e.type == KEYUP: 
        data['key'] = e.key 
        data['mod'] = e.mod

    if e.type == MOUSEMOTION:	 
        data['pos'] = e.pos 
        data['rel'] = e.rel 
        data['buttons'] = e.buttons

    if e.type == MOUSEBUTTONUP: 
        data['pos'] = e.pos 
        data['button'] = e.button

    if e.type == MOUSEBUTTONDOWN: 
        data['pos'] = e.pos 
        data['button'] = e.button

    if e.type == JOYAXISMOTION: 
        data['joy'] = e.joy 
        data['axis'] = e.axis 
        data['value'] = e.value

    if e.type == JOYBALLMOTION: 
        data['joy'] = e.joy 
        data['ball'] = e.ball 
        data['rel'] = e.rel

    if e.type == JOYHATMOTION: 
        data['joy'] = e.joy 
        data['hat'] = e.hat 
        data['value'] = e.value

    if e.type == JOYBUTTONUP: 
        data['joy'] = e.joy 
        data['button'] = e.button

    if e.type == JOYBUTTONDOWN: 
        data['joy'] = e.joy 
        data['button'] = e.button

    if e.type == VIDEORESIZE: 
        data['size'] = e.size 
        data['w'] = e.w 
        data['h'] = e.h

    if e.type == VIDEOEXPOSE: 
        data['none'] = ''

    if e.type == USEREVENT: 
        data['code'] = e.code

    type = ''

    if e.type == QUIT: type = "QUIT"
    if e.type == ACTIVEEVENT: type = "ACTIVEEVENT"
    if e.type == KEYDOWN: type = "KEYDOWN"
    if e.type == KEYUP: type = "KEYUP"
    if e.type == MOUSEMOTION    : type = "MOUSEMOTION"
    if e.type == MOUSEBUTTONUP: type = "MOUSEBUTTONUP"
    if e.type == MOUSEBUTTONDOWN: type = "MOUSEBUTTONDOWN"
    if e.type == JOYAXISMOTION: type = "JOYAXISMOTION"
    if e.type == JOYBALLMOTION: type = "JOYBALLMOTION"
    if e.type == JOYHATMOTION: type = "JOYHATMOTION"
    if e.type == JOYBUTTONUP: type = "JOYBUTTONUP"
    if e.type == JOYBUTTONDOWN: type = "JOYBUTTONDOWN"
    if e.type == VIDEORESIZE: type = "VIDEORESIZE"
    if e.type == VIDEOEXPOSE: type = "VIDEOEXPOSE"
    if e.type == USEREVENT: type = "USEREVENT"

    return [type, data]

def d():
    glBegin(GL_TRIANGLES)				

    glColor3f(1.0,0.0,0.0)
    glVertex3f( 0.0, 1.0, 0.0)		
    glColor3f(0.0,1.0,0.0)
    glVertex3f(-1.0,-1.0, 1.0)
    glColor3f(0.0,0.0,1.0)	
    glVertex3f( 1.0,-1.0, 1.0)
    
    glColor3f(1.0,0.0,0.0)	
    glVertex3f( 0.0, 1.0, 0.0)
    glColor3f(0.0,0.0,1.0)	
    glVertex3f( 1.0,-1.0, 1.0)
    glColor3f(0.0,1.0,0.0)	
    glVertex3f( 1.0,-1.0, -1.0)

    glColor3f(1.0,0.0,0.0)	
    glVertex3f( 0.0, 1.0, 0.0)
    glColor3f(0.0,1.0,0.0)	
    glVertex3f( 1.0,-1.0, -1.0)
    glColor3f(0.0,0.0,1.0)	
    glVertex3f(-1.0,-1.0, -1.0)
		
		
    glColor3f(1.0,0.0,0.0)	
    glVertex3f( 0.0, 1.0, 0.0)
    glColor3f(0.0,0.0,1.0)	
    glVertex3f(-1.0,-1.0,-1.0)
    glColor3f(0.0,1.0,0.0)	
    glVertex3f(-1.0,-1.0, 1.0)
    glEnd()

