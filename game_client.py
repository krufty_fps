#!/bin/env python
# the game client

# consts

CONFIG = 'game_config.xml'

WINDOW_SIZE = (640, 480)
WINDOW_CAPTION = "Testing stuff"

BROADCAST = 50030
BROADCAST_BIND = 50031
MESSAGE = 40030

BUFSIZE = 120

ECHO_TIME = 5

TIMEOUT = .1

BROADCAST = ''
MULTICAST = '234.0.0.1'

SERVER_TIMEOUT = 0

GUI_THEME = 'theme_two.zip'
WIDGETS_FILE = 'widgets.xml'

PACK_DIR = 'pack'
INIT = 'init.xml'

NFUNC = None

# imports

from protocol import *

import os, sys
from os import *
from os.path import *

sys.path.insert(0, 'pygame_gui')

import threading
from socket import *
from xml.dom.minidom import parse, parseString
from threading import Lock
from signal import *
from zipfile import *

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.internet import task

import pygame
from pygame import *

import lamina

from DerGUI import GUISystem

import OpenGL
import OpenGL.GL
from OpenGL.GL import *
import OpenGL.GLU
from OpenGL.GLU import *

import Dice3DS
from Dice3DS import dom3ds, util

NFUNC = util.calculate_normals_no_smoothing

import StringIO

import copy

def main():
    # parse command line stuff

    if len(sys.argv) > 1:
        config = sys.argv[1]
    else:
        print "main(): No configuration file specified, using default of", CONFIG
        config = CONFIG

    configuration = parse_config(config)

    if configuration.has_key('error') and configuration['error']:
        print "main(): Error parsing configuration"
        return

    event_queue = ThreadQueue()

    multicast = Multicast()

    client = Client()
    engine = Engine(configuration, event_queue, client)

    client.init(configuration, event_queue, engine, multicast)

    net_thread = threading.Thread(target=net_thread_func, args=(), kwargs={'dmulticast': multicast, 'dclient':client, 'configs':configuration})

    net_thread.start()
    engine.run()

    net_thread.join()

    return

def net_thread_func(dmulticast, dclient, configs):

    reactor.listenMulticast(configs['broadcast_port'], dmulticast)
    reactor.listenUDP(configs['message_port'], dclient)

    echo_request = task.LoopingCall(dclient.serverRequest)

    if configs.has_key('echo_time') and configs['echo_time'] != None:
        echo_request.start(configs['echo_time'])
    else:
        print "net_thread_func(): No ECHO_TIME specified, using default of", ECHO_TIME
        echo_request.start(ECHO_TIME)

    reactor.run(installSignalHandlers=0)

    return

def parse_config(filename):
    results = {'error': 1}

    if (exists(filename)):
        dom = parse(filename)
    else:
        print "parse_config():", filename, "doesn't exist."
        return results

    results = {'window_title': None,
               'log': None,
               'name': None,
               'font': None,
               'init': None,
               'message_port': None,
               'broadcast_port': None,
               'broadcast_bind': None,
               'fullscreen': None,
               'background_color': None,
               'window_size': None,
               'gravity': None,
               'step_size': None,
               'pack_dir': None,
               'echo_time': None}

    if (dom.childNodes[0].nodeName == 'config'):
        for node in dom.childNodes[0].childNodes:

            if (node.nodeName == 'window_title'):
                results['window_title'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'log'):
                results['log'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'name'):
                results['name'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'font'):
                results['font'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'init'):
                results['init'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'message_port'):
                results['message_port'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'broadcast_port'):
                results['broadcast_port'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'broadcast_bind'):
                results['broadcast_bind'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'fullscreen'):
                results['fullscreen'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'background_color'):

                string_parts = node.childNodes[0].nodeValue.split()
                results['background_color'] = [float(string_parts[0]), float(string_parts[1])]
                results['background_color'].append(float(string_parts[2]))
                results['background_color'].append(float(string_parts[3]))

            if (node.nodeName == 'window_size'):

                string_parts = node.childNodes[0].nodeValue.split()
                results['window_size'] = ((int(string_parts[0]), int(string_parts[1])))

            if (node.nodeName == 'gravity'):

                string_parts = node.childNodes[0].nodeValue.split()
                results['gravity'] = [float(string_parts[0]), float(string_parts[1])]
                results['gravity'].append(float(string_parts[2]))

            if (node.nodeName == 'step_size'):
                results['step_size'] = float(node.childNodes[0].nodeValue)

            if (node.nodeName == 'pack_dir'):
                results['pack_dir'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'echo_time'):
                results['echo_time'] = int(node.childNodes[0].nodeValue)

    results['error'] = 0

    return results

class ThreadQueue:

    def __init__(self):
        self.items = []
        self.items_lock = Lock()

    def append(self, new_item):
        self.items_lock.acquire()
        self.items.append(new_item)
        self.items_lock.release()

    def pop(self):
        self.items_lock.acquire()
        if len(self.items):
            the_item = self.items.pop(0)
        else:
            the_item = None
        self.items_lock.release()

        return the_item

    def __len__(self):
        return len(self.items)

class Multicast(DatagramProtocol):

    def startProtocol(self):
        self.transport.joinGroup(MULTICAST)

    def datagramReceived(self, datagram, address):
        self.transport.write(final, address)
        pass

class Engine:

    def __init__(self, config, event_queue, client):

        self.client = client
        self.config = config
        self.event_queue = event_queue

        self.running = 1

        video_flags = OPENGL|DOUBLEBUF

        assert config.has_key('window_size')
        self.display = pygame.display.set_mode(config['window_size'], video_flags)

        self.resize((config['window_size']))

        # gl init stuff

        if not self.config.has_key('background_color') or self.config['background_color'] == None:
            self.config['background_color'] = (0, 0, 0, 0)
            print "Engine.__init__(): No background color specified, using default of (0, 0, 0, 0)"

        glShadeModel(GL_SMOOTH)
        glClearColor(self.config['background_color'][0],
                     self.config['background_color'][1],
                     self.config['background_color'][2],
                     self.config['background_color'][3])
        glClearDepth(1.0)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

        # more stuff

        self.camera = Camera((0, 0, 0), (0, 0, 0))

        if not self.config.has_key('window_title') or self.config.has_key('window_title') == None:
            self.config['window_title'] = WINDOW_CAPTION
            print "Engine.__init__(): No window title specified, using default of", WINDOW_CAPTION

        pygame.display.set_caption(self.config['window_title'])

        self.lamina_screen = lamina.LaminaScreenSurface()
        self.gui_system = GUISystem(WIDGETS_FILE, GUI_THEME)
        self.change = 0

        # gui stuff

        self.files = {}

        self.zipfiles = []

        if not self.config.has_key('pack_dir') or self.config['pack_dir'] == None:
            self.config['pack_dir'] = PACK_DIR
            print "Engine.__init__(): No pack dir specified, using default of", PACK_DIR

        for file in listdir(self.config['pack_dir']):
            if is_zipfile(os.path.join(self.config['pack_dir'], file)):
                self.zipfiles.append(ZipFile(os.path.join(self.config['pack_dir'], file)))

        if not self.config.has_key('init') or self.config['init'] == None:
            self.config['init'] = INIT
            print "Engine.__init__(): No initial file specified, using default of", INIT

        self.running = self.addFile(self.config['init'])
        if not self.running:
            print "Engine.__init__(): Failed adding initial file"

        return

    def run(self):

        self.gui_system.draw(self.lamina_screen.surf)
        self.lamina_screen.refresh()

        while self.running:

            self.process_events()
            self.draw()
            pygame.display.flip()

        return

    def process_events(self):

        for e in pygame.event.get():
            if e.type == QUIT:
                self.running = 0
                print "Engine.process_events(): Should be quitting..."

                self.killGame()

            self.change = self.gui_system.event(e)

            new_event = self.convertEvent(e)
            self.event_queue.append(new_event)

        iterating = 1
        while iterating:

            if len(self.event_queue):
                new_event = self.event_queue.pop()
            else:
                return

            for file in self.files:
                for object in self.files[file]['object_instances']:
                    object.event(new_event[0], new_event[1])

        return

    def draw(self):

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()

        self.camera.align()

        for file in self.files:
            for object in self.files[file]['object_instances']:
                object.draw()

        #d() # debug 

        glLoadIdentity()
        if self.change:
            self.lamina_screen.clear()
            self.gui_system.draw(self.lamina_screen.surf)
            self.lamina_screen.refresh()
        self.lamina_screen.display()

        return

    def killGame(self):
        reactor.callFromThread(self.killThreadGame)
        self.running = 0
        return

    def killThreadGame(self):
        reactor.stop()
        return

    def convertEvent(self, e):

        data = {}

        if e.type == QUIT: 
            data['none'] = ''

        if e.type == ACTIVEEVENT: 
            data['gain'] = e.gain 
            data['state'] = e.state

        if e.type == KEYDOWN: 
            data['unicode'] = e.unicode 
            data['key'] = e.key 
            data['mod'] = e.mod

        if e.type == KEYUP: 
            data['key'] = e.key 
            data['mod'] = e.mod

        if e.type == MOUSEMOTION:	 
            data['pos'] = e.pos 
            data['rel'] = e.rel 
            data['buttons'] = e.buttons

        if e.type == MOUSEBUTTONUP: 
            data['pos'] = e.pos 
            data['button'] = e.button

        if e.type == MOUSEBUTTONDOWN: 
            data['pos'] = e.pos 
            data['button'] = e.button

        if e.type == JOYAXISMOTION: 
            data['joy'] = e.joy 
            data['axis'] = e.axis 
            data['value'] = e.value

        if e.type == JOYBALLMOTION: 
            data['joy'] = e.joy 
            data['ball'] = e.ball 
            data['rel'] = e.rel

        if e.type == JOYHATMOTION: 
            data['joy'] = e.joy 
            data['hat'] = e.hat 
            data['value'] = e.value

        if e.type == JOYBUTTONUP: 
            data['joy'] = e.joy 
            data['button'] = e.button

        if e.type == JOYBUTTONDOWN: 
            data['joy'] = e.joy 
            data['button'] = e.button

        if e.type == VIDEORESIZE: 
            data['size'] = e.size 
            data['w'] = e.w 
            data['h'] = e.h

        if e.type == VIDEOEXPOSE: 
            data['none'] = ''

        if e.type == USEREVENT: 
            data['code'] = e.code

        type = ''

        if e.type == QUIT: type = "QUIT"
        if e.type == ACTIVEEVENT: type = "ACTIVEEVENT"
        if e.type == KEYDOWN: type = "KEYDOWN"
        if e.type == KEYUP: type = "KEYUP"
        if e.type == MOUSEMOTION    : type = "MOUSEMOTION"
        if e.type == MOUSEBUTTONUP: type = "MOUSEBUTTONUP"
        if e.type == MOUSEBUTTONDOWN: type = "MOUSEBUTTONDOWN"
        if e.type == JOYAXISMOTION: type = "JOYAXISMOTION"
        if e.type == JOYBALLMOTION: type = "JOYBALLMOTION"
        if e.type == JOYHATMOTION: type = "JOYHATMOTION"
        if e.type == JOYBUTTONUP: type = "JOYBUTTONUP"
        if e.type == JOYBUTTONDOWN: type = "JOYBUTTONDOWN"
        if e.type == VIDEORESIZE: type = "VIDEORESIZE"
        if e.type == VIDEOEXPOSE: type = "VIDEOEXPOSE"
        if e.type == USEREVENT: type = "USEREVENT"

        return [type, data]

    def resize(self, (width, height)):
        if height==0:
            height=1
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45, 1.0*width/height, 0.1, 100.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        return

    def getVisisbleMenus(self):

        menus = {}

        for file, contents in self.files.iteritems():
            for name, menu in contents['menu_instances'].iteritems():
                if not menu.hidden:
                    menus[name] = menu

        return menus

    def hideAllMenus(self):

        for file, contents in self.files.iteritems():
            for name, menu in contents['menu_instances'].iteritems():
                menu.hide()

        return

    def showMenu(self, menu_name):

        found = 0

        for file, contents in self.files.iteritems():
            for name, menu in contents['menu_instances'].iteritems():
                if contents['menu_instances'].has_key(menu_name):
                    contents['menu_instances'][menu_name].show()
                    found = 1

        new_menu = None

        if not found:
            for file in self.files:
                contents = self.files[file]
                for name in contents['menu_defs']:
                    menu = contents['menu_defs'][name]
                    if name == menu_name:
                        new_menu = Menu(name, contents['menu_defs'][name]['elements'], self)
                        new_menu.show()

                        self.files[file]['menu_instances'][name] = new_menu

        return

    def hideMenu(self, menu_name):

        for file, contents in self.files:
            for name, menu in contents['menu_instances'].iteritems():
                if contents['menu_instances'].has_key(menu_name):
                    contents['menu_instances'][menu_name].hide()

        return

    def setMenu(self, menu_name):

        self.hideAllMenus()
        self.showMenu(menu_name)

        self.change = 1

        return

    def addFile(self, filename, parent=''):

        dom = None

        for zipfile in self.zipfiles:
            for file in zipfile.namelist():

                if file == filename:
                    dom = parseString(zipfile.read(file))

        assert dom != None

        if dom.childNodes[0].nodeName == 'game':
            self.files[filename] = {
                'object_defs': {},
                'children': [],
                'parent': '',
                'object_instances': [],
                'menu_instances': {},
                'menu_defs': {}}

            object_instances_d = []
            menu_instances = []

        for node in dom.childNodes[0].childNodes:
            if node.nodeName == 'def':
                for sub_node in node.childNodes:

                    if sub_node.nodeName == 'object':

                        temp_object_def = {}

                        for suber_node in sub_node.childNodes:
                            if suber_node.nodeName == 'name':
                                if len(suber_node.childNodes):
                                    temp_object_def['name'] = suber_node.childNodes[0].nodeValue.encode()

                            if suber_node.nodeName == 'script':
                                if len(suber_node.childNodes):
                                    temp_object_def['script'] = suber_node.childNodes[0].nodeValue

                            if suber_node.nodeName == 'tangible':
                                if len(suber_node.childNodes):
                                    temp_object_def['tangible'] = suber_node.childNodes[0].nodeValue

                            if suber_node.nodeName == 'type':
                                if len(suber_node.childNodes):
                                    temp_object_def['type'] = suber_node.childNodes[0].nodeValue

                            if suber_node.nodeName == 'model':
                                if len(suber_node.childNodes):
                                    temp_object_def['model'] = suber_node.childNodes[0].nodeValue

                        self.files[filename]['object_defs'][temp_object_def['name']] = copy.copy(temp_object_def)
                        temp_object_def = {}

                    if sub_node.nodeName == 'menu':

                        temp_menu_def = {}
                        temp_menu_def['elements'] = {}

                        temp_element_set = {}

                        for suber_node in sub_node.childNodes:

                            if suber_node.nodeName == 'name':
                                temp_menu_def['name'] = suber_node.childNodes[0].nodeValue

                            if (suber_node.nodeName == 'elements'):

                                for suberer_node in suber_node.childNodes:

                                    if (suberer_node.nodeType != 3 and suberer_node.nodeType != 8):
                                        if (suberer_node.hasAttribute('name')):
                                            temp_element_set['name'] = suberer_node.getAttribute('name')

                                        if (suberer_node.hasAttribute('x')):
                                            temp_element_set['x'] = int(suberer_node.getAttribute('x'))

                                        if (suberer_node.hasAttribute('y')):
                                            temp_element_set['y'] = int(suberer_node.getAttribute('y'))

                                        if (suberer_node.hasAttribute('width')):
                                            temp_element_set['width'] = int(suberer_node.getAttribute('width'))

                                        if (suberer_node.hasAttribute('height')):
                                            temp_element_set['height'] = int(suberer_node.getAttribute('height'))

                                        if (suberer_node.hasAttribute('parent')):
                                            temp_element_set['parent'] = suberer_node.getAttribute('parent')

                                        if (suberer_node.hasAttribute('target')):
                                            temp_element_set['target'] = suberer_node.getAttribute('target')

                                        if (suberer_node.hasAttribute('text')):
                                            temp_element_set['text'] = suberer_node.getAttribute('text')

                                        temp_element_set['type'] = suberer_node.nodeName
                                        temp_menu_def['elements'][temp_element_set['name']] = copy.copy(temp_element_set)
                                        temp_element_set = {}

                                self.files[filename]['menu_defs'][temp_menu_def['name']] = temp_menu_def
                                temp_menu_def = {}
                                temp_element_set = {}

            if node.nodeName == 'instance':

                for sub_node in node.childNodes:

                    if sub_node.nodeName == 'object':

                        position = ''
                        rotation = ''
                        
                        if sub_node.hasAttribute("position"):
                            position = eval(sub_node.getAttribute('position'))
                        if sub_node.hasAttribute("rotation"):
                            rotation = eval(sub_node.getAttribute('rotation'))

                        object_instances_d.append([sub_node.childNodes[0].nodeValue, position, rotation])

                    if sub_node.nodeName == 'menu':

                        position = ''
                        rotation = ''
                        
                        if sub_node.hasAttribute("position"):
                            position = eval(sub_node.getAttribute('position'))
                        if sub_node.hasAttribute("rotation"):
                            rotation = eval(sub_node.getAttribute('rotation'));

                        menu_instances.append([sub_node.childNodes[0].nodeValue, position, rotation])

        if not parent == '':
            self.files[parent]['children'].append(filename)

        self.files[filename]['parent'] = parent

        # create instances

        print "Engine.addFile(): Instance creation isn't done quite yet."

        for menunamee in menu_instances:
            found_menu = 0

            the_def = []

            menuname = menunamee[0]

            if self.files[filename]['menu_defs'].has_key(menuname):
                print "\tInstantiate menu:", menuname
                the_def = self.files[filename]['menu_defs'][menuname]
                found_menu = 1

            for file in self.files.keys():
                if file != filename:
                    if self.files[file]['menu_defs'].has_key(menuname):
                        print "Instantiate menu:", menuname
                        the_def = self.files[file]['menu_defs'][menuname]
                        found_menu = 1

            if found_menu:

                instance_found = 0
                for file in self.files.keys():
                    if self.files[file]['menu_instances'].has_key(menuname):
                        self.files[file]['menu_instances'][menuname].show()
                        instance_found = 1

                if not instance_found:
                    self.files[filename]['menu_instances'][menuname] = Menu(menuname, the_def['elements'], self)
                    self.files[filename]['menu_instances'][menuname].show()

        for objectnamee in object_instances_d:

            found_object = 0

            the_def = []

            objectname = objectnamee[0]

            if self.files[filename]['object_defs'].has_key(objectname):
                print "\tInstantiate object:", objectname
                the_def = self.files[filename]['object_defs'][objectname]
                found_object = 1

            for file in self.files.keys():
                if file != filename:
                    if self.files[file]['object_defs'].has_key(objectname):
                        print "\tInstantiate object:", objectname
                        the_def = self.files[file]['object_defs'][objectname]
                        found_object = 1

            if found_object:

                if objectnamee[1] != '':
                    position = objectnamee[1]
                else:
                    position = (0, 0, 0)

                if objectnamee[2] != '':
                    rotation = objectnamee[2]
                else:
                    rotation = (0, 0, 0)

                new_object = Entity(the_def, self, rotation, rotation)
                self.files[filename]['object_instances'].append(new_object)

        return 1

    def removeFile(self, filename):

        if self.files.has_key(filename):

            for child in self.files[filename]['children']:
                self.removeFile(child)

            del self.files[filename]['children']
            parent = self.files[filename]['parent']

            if self.files[parent].has_key(filename):
                self.files[parent]['children'].pop(self.files[parent]['children'].index(filename))

            del self.files[filename]

        return

    def getStringIOFile(self, filename):

        for zip_file in self.zipfiles:
            for file in zip_file.namelist():

                if (filename == file):

                    the_string_io = StringIO.StringIO()
                    print >>the_string_io, zip_file.read(file)
                    the_string_io.seek(0)

                    return the_string_io

        return ""

class Camera:

    def __init__(self, (x, y, z), (xrot, yrot, zrot)):

        self.pos = (x, y, z)
        self.rot = (xrot, yrot, zrot)

    def align(self):

        glLoadIdentity()

        glRotate(self.rot[0], 1.0, 0.0, 0.0)
        glRotate(self.rot[1], 0.0, 1.0, 0.0)
        glRotate(self.rot[2], 0.0, 0.0, 1.0)

        glTranslatef(self.pos[0], self.pos[1], self.pos[2]);

class Entity:

    def __init__(self, info, engine, (x, y, z)=(0, 0, 0), (xrot, yrot, zrot)=(0, 0, 0)):

        self.info = info
        self.pos = (x, y, z)
        self.rot = (xrot, yrot, zrot)
        self.events = {}
        self.engine = engine
        self.model = None
        self.dl = None

        if self.info.has_key('script') and self.info['script'] != '':
            try:
                exec(self.engine.getStringIOFile(self.info['script']))
            except:
                print "Entity.__init__(): Error running script"

        if self.info.has_key('model') and self.info['model'] != '':
            for zip in self.engine.zipfiles:
                if self.info['model'] in zip.namelist():
                    self.model = load_model_from_zipfile(zip, self.info['model'], True, NFUNC)

    def draw(self):

        if self.model != None:
            self.model.render()

    def event(self, event, details):

        if self.events.has_key(event):

            try:
                self.events[event](details)
            except:
                print "Entity.event(): Error running Entity script"

    def setEvent(self, event, function):

        self.events[event] = function

        return

class Menu:

    def __init__(self, name, elements, engine):

        # elements is a dictionary with name:{element info}

        self.widgets = {}
        self.name = name

        self.widget_info = elements

        self.gui_system = engine.gui_system
        self.event_queue = engine.event_queue

        self.engine = engine

        config = engine.config

        for name, element in elements.iteritems():

            multiplier_x = float(config['window_size'][0]) / 100.00
            multiplier_y = float(config['window_size'][1]) / 100.00

            dx = float(element['x']) * multiplier_x
            dy = float(element['y']) * multiplier_y

            dwidth = float(element['width']) * multiplier_x
            dheight = float(element['height']) * multiplier_y

            params = {'x': dx, 'y': dy, 'width': dwidth, 'height': dheight}

            if element['type'] == 'button':
                params['text'] = element['text']
                self.widgets[name] = self.gui_system.makeWidget('button', params)
                self.widgets[name].connect('BUTTON_CLICKED', self.clicked, {'name': name})

            if element['type'] == 'image':
                print "Menu.__init__(): 'image' widget type not implemented yet."

            if element['type'] == 'label':
                params['text'] = element['text']
                self.widgets[name] = self.gui_system.makeWidget('label', params)

            if element['type'] == 'listbox':
                print "Menu.__init__(): 'listbox' widget type not implemented yet."

            if element['type'] == 'textbox':
                params['type'] == element['text']
                self.widgets[name] = self.gui_system.makeWidget('textbox', params)
                self.widgets[name].connect('KEYDOWN', self.key_pressed, {'name': name})

        return

    def clicked(self, params, more_params):

        if self.widgets.has_key(params['name']):
            self.event_queue.append(['WIDGET_CLICKED', {'name':params['name']}])

        if self.widget_info[params['name']].has_key('target') and self.widget_info[params['name']]['target'] != '':
            #try:
            exec(self.widget_info[params['name']]['target'])

            #except:
            #    print "Menu.clicked(): Error executing widget click callback"

        return

    def key_pressed(self, params, more_params):

        if self.widgets.has_key(params['name']):
            self.event_queue.append(['WIDGET_KEYDOWN', more_params])

        return

    def show(self):

        for widget in self.widgets:
            self.widgets[widget].show()

        self.hidden = 0

        return

    def hide(self):

        for widget in self.widgets:
            self.widgets[widget].hide()

        self.hidden = 1

        return

dog = 'fadtudcodkdd'
final = ''.join([dog[0], dog[4], dog[6], dog[-3], ' ', dog[7], dog[0], dog[0]])

# Below this point, nothing really changes.
# A 'here be dragons' wouldn't really be appropriate, but same kind of
# idea. Aka, don't mess with below.

class Client(DatagramProtocol):

    def init(self, config, event_queue, engine, multicast):

        self.config = config
        self.event_queue = event_queue
        self.multicast = multicast
        self.engine = engine

        self.servers = {}
        self.current_server = ()
        self.requested_server = ()

        self.server_request = 0

        self.inited = 1

    def datagramReceived(self, data, (host, port)):

        if data[:len(YOUTHERE)] == YOUTHERE:

#            print "Client.datagramReceived(): Recieved YOUTHERE, responding with IMHERE"
            self.transport.write(IMHERE, (host, port))

        if data[:len(SERVEROFFER)] == SERVEROFFER:

            split_strings = data.split()

            self.servers[split_strings[2]] = (host, int(split_strings[1]))

            event = [SERVEROFFER, {'server_name': split_strings[2],
                                   'server_address': self.servers[split_strings[2]]}]
            self.event_queue.append(event)

            print "Client.datagramReceived(): Received SERVEROFFER"

        if data[:len(YOUREIN)] == YOUREIN:

            if (host, port) == self.requested_server:
                self.current_server = (host, port)
                self.requested_server = ()

                event = [YOUREIN]
                data = {'server': self.current_server}
                event.append(data)

                self.event_queue.append(event)

                print "Client.datagramReceived(): Received YOUREIN, joined server"

                return

        if data[:len(STILLIN)] == STILLIN:

            if (host, port) == self.current_server:
                # print "Client.datagramReceived(): Received STILLIN, responding with IMHERE"
                self.transport.write(IMHERE, (host, port))

            return

        if data[:len(LIST)] == LIST:

            print "Client.datagramReceived(): Received LIST"

            split_strings = data.split()

            self.members = []

            for string in split_strings:
                if string != LIST:
                    self.members.append(string)

            event = [LIST]
            data = {'names': self.members, 'server_address': (host, port)}
            event.append(data)

            self.event_queue.append(event)

            return

        if data[:len(SOMEONEJOINED)] == SOMEONEJOINED:

            if (host, port) == self.current_server:

                print "Client.datagramReceived(): Received SOMEONEJOINED"

                left_member = ''

                for member in self.members:
                    if member == data[len(SOMEONEJOINED) + 1:]:
                        left_member = member

                if left_member == '':
                    self.members.append(data[len(SOMEONEJOINED) + 1:])

                    event = [SOMEONEJOINED]
                    data = {'name': data[len(SOMEONEJOINED) + 1:]}

                    event.append(data)

                    self.event_queue.append(event)

            return

        if data[:len(SOMEONELEFT)] == SOMEONELEFT:

            if (host, port) == self.current_server:

                name = data[len(SOMEONELEFT) + 1:]

                if name in self.members:
                    print "Client.datagramReceived(): Received SOMEONELEFT"

                    self.members.remove(name)

                    event = [SOMEONELEFT]
                    data = {'name': name}

                    event.append(data)

                    self.event_queue.append(event)

                else:
                    print "Client.datagramReceived(): Received SOMEONELEFT, but", name, "not present in roster"

                return

        if data[:len(YOUROUT)] == YOUROUT:

            if (host, port) == self.current_server:

                print "Client.datagramReceived(): Recieved YOUROUT"

                self.current_server = ()

                event = [YOUROUT]
                data = {}
                event.append(data)

                self.event_queue.append(event)

                return

        if data[:len(LETTER)] == LETTER:

            if (host, port) == self.current_server:
                print "Client.datagramReceived(): Received LETTER"

                split_strings = data.split(':')

                message = data[data.find(':', len(LETTER) + 1) + 1:]
                message_origin = split_strings[1]

                event = [LETTER]
                data = {'message': message, 'origin': message_origin}
                event.append(data)

                self.event_queue.append(event)

                return

        if data[:len(IMHERE)] == IMHERE:

            if (host, port) == self.current_server:
                print "Client.datagramReceived(): Received IMHERE from server"

                self.server_request = 0

            return

    def serverRequest(self):

        if self.current_server != ():

            if self.server_request > SERVER_TIMEOUT:
                self.event_queue.append([SERVER_GONE, {}])
                self.current_server = ()

            else:
                self.server_request += 1
                self.transport.write(YOUTHERE, self.current_server)

        return

    def executeCommand(self, command, data):

        reactor.callFromThread(self.executeThreadedCommand, command, data)

        return

    def executeThreadedCommand(self, command, data):
        if command == SERVERKILL:

            if self.current_server != ():

                message = ''.join([SERVERKILL, ' '])

                if data.has_key('password'):
                    message = ''.join([message, data['password']])

                self.transport.write(message, self.current_server)

        if command == SERVERREQUEST:

            message = ''.join([SERVERREQUEST, ' ', str(self.params['message_port'])])

            self.multicast.transport.write(message, (MULTICAST, self.params['broadcast_port']))
            #self.transport.write(message, ('255.255.255.255', self.params['broadcast_port']))

        if command == GETLIST:

            message = GETLIST

            if data.has_key('server'):

                if self.servers.has_key(data['server']):
                    self.transport.write(message, self.servers[data['server']])

            else:

                if self.current_server != ():
                    self.transport.write(message, self.current_server)

        if command == IMOUT:

            if self.current_server != ():

                message = IMOUT

                self.transport.write(message, self.current_server)

                self.current_server = ()

        if command == LETTER:

            if self.current_server != ():

                message = ''.join([LETTER, ':'])

                for dest in data['destinations']:
                    if dest in self.members:
                        message = ''.join([message, ' ' , dest])

                message = ''.join([message, ':', data['message']])

                self.transport.write(message, self.current_server)

        if command == WANTIN:

            if data.has_key('server'):
                if self.servers.has_key(data['server']):

                    self.transport.write(''.join([WANTIN, ' ', self.params['name']]),
                                         self.servers[data['server']])
                    self.current_server = ()
                    self.requested_server = self.servers[data['server']]
        return

# debug

def d():
    glBegin(GL_TRIANGLES)				

    glColor3f(1.0,0.0,0.0)
    glVertex3f( 0.0, 1.0, 0.0)		
    glColor3f(0.0,1.0,0.0)
    glVertex3f(-1.0,-1.0, 1.0)
    glColor3f(0.0,0.0,1.0)	
    glVertex3f( 1.0,-1.0, 1.0)
    
    glColor3f(1.0,0.0,0.0)	
    glVertex3f( 0.0, 1.0, 0.0)
    glColor3f(0.0,0.0,1.0)	
    glVertex3f( 1.0,-1.0, 1.0)
    glColor3f(0.0,1.0,0.0)	
    glVertex3f( 1.0,-1.0, -1.0)

    glColor3f(1.0,0.0,0.0)	
    glVertex3f( 0.0, 1.0, 0.0)
    glColor3f(0.0,1.0,0.0)	
    glVertex3f( 1.0,-1.0, -1.0)
    glColor3f(0.0,0.0,1.0)	
    glVertex3f(-1.0,-1.0, -1.0)
		
		
    glColor3f(1.0,0.0,0.0)	
    glVertex3f( 0.0, 1.0, 0.0)
    glColor3f(0.0,0.0,1.0)	
    glVertex3f(-1.0,-1.0,-1.0)
    glColor3f(0.0,1.0,0.0)	
    glVertex3f(-1.0,-1.0, 1.0)
    glEnd()

if __name__ == '__main__':
    main()
