#!/usr/bin/python

import os, sys
from os.path import *

import threading
from socket import *
from xml.dom.minidom import parse, parseString
from threading import Lock
from signal import *

CONFIG = 'config.xml'

BROADCAST = 50030
MESSAGE = 40031
PASSWORD = "changethis"
NAME = "default"

BUFSIZE = 120

# this will change

SERVERREQUEST = "i_want_server"
SERVEROFFER = "Want_server?"
SERVERKILL = "DIE_server!!"
YOUTHERE = "you_there?"
IMHERE = "yeah,i'm_here"
WANTIN = "i_want_in"
YOUREIN = "urine"
IMOUT = "i_leave"
GETLIST = "get_list"
LIST = "peoples_on_server"
SOMEONEJOINED = "dude,someone_joined"
SOMEONELEFT = "someone_left"
YOUROUT = "get_lost_punk"
LETTER = "listen_to_me"

# end changes

TIMEOUT = .1

my_server = 0

class Server:

    def __init__(self, config):

        self.running = 1

        self.outgoing = []
        self.incoming = []

        self.outgoing_lock = Lock()
        self.incoming_lock = Lock()

        self.broadcast = socket(AF_INET, SOCK_DGRAM)
        self.message = socket(AF_INET, SOCK_DGRAM)

        self.message_lock = Lock()

        self.members = {}

        self.configuration = config

        if (config.has_key('broadcast')):
            self.broadcast.bind(('', config['broadcast']))
        else:
            print "Server.__init__(): No broadcast socket number chosen, using", BROADCAST
            self.broadcast.bind(('', BROADCAST))

        if (config.has_key('message')):
            self.message.bind(('', config['message']))
        else:
            print "Server.__init__(): No message socket number chosen, using", MESSAGE
            self.message.bind(('', MESSAGE))

    def receive(self):

        print "Server.receive() beginning..."

        self.message.settimeout(TIMEOUT)

        while (self.running):
            try:
                self.message_lock.acquire()
                data_in = self.message.recvfrom(BUFSIZE)
                self.message_lock.release()
            except:
                data_in = ()

            if (data_in != ()):
                self.incoming_lock.acquire()
                self.incoming.append(data_in)
                print "Got data", data_in
                self.incoming_lock.release()

            return

    def send(self):

        print "Server.send() beginning..."

        while (self.running):

            if (len(self.outgoing)):

                self.outgoing_lock.acquire()
                print "Server.send(): Acquired outgoing_lock"

                data_out = self.outgoing.pop(0)

                self.outgoing_lock.release()
                print "Server.send(): Released outgoing_lock"

                self.message.sendto(data_out[0], data_out[1])
                print "Server.send(): Sent data", data_out
        return

    def process_in(self):

        print "Server.process_in() beginning..."

        while (self.running):

            if (len(self.incoming)):
                # data in = ()

                self.incoming_lock.acquire()
                print "Server.process_in(): Acquired incoming_lock"

                data_in = self.incoming.pop(0)

                self.incoming_lock.release()
                print "Server.process_in(): Released incoming_lock"

                if (data_in[0][:len(WANTIN)] == WANTIN):
                    print "Server.process_in(): Received WANTIN"

                    response = [YOUREIN, data_in[1]]

                    self.outgoing_lock.acquire()
                    print "Server.process_in(): Acquired outgoing_lock"

                    self.outgoing.append(response)

                    self.outgoing_lock.release()
                    print "Server.process_in(): Released outgoing_lock"

                    self.members[data_in[0][len(WANTIN) + 1:]] = data_in[1]

                if (data_in[0][:len(YOUTHERE)] == YOUTHERE):
                    print "Server.process_in(): Received YOUTHERE, responding with IMHERE"

                    response = [IMHERE, data_in[1]]

                    self.outgoing_lock.acquire()
                    print "Server.process_in(): Acquired outgoing_lock"

                    self.outgoing.append(response)

                    self.outgoing_lock.release()
                    print "Server.process_in(): Released outgoing_lock"

                if (data_in[0][:len(IMOUT)] == IMOUT):
                    print "Server.process_in(): Received IMOUT, notifying clients"

                    response = []
                    name_to_delete = ''

                    for name, address in self.members.iteritems():
                        if (data_in[1] == address):
                            response[0] = SOMEONELEFT + " " + name
                            name_to_delete = name

                    if (name_to_delete != ''):
                        del self.members[name_to_delete]

                        for name, address in self.members.iteritems():
                            response[1] = address

                            self.outgoing_lock.acquire()
                            print "Server.process_in(): Acquired outgoing_lock"

                            self.outgoing.append(response)

                            self.outgoing_lock.release()
                            print "Server.process_in(): Released outgoing_lock"

                if (data_in[0] == GETLIST):
                    print "Server.process_in(): Received GETLIST, responding with member list"

                    for name, address in self.members.iteritems():
                        if (address == data_in[1]):
                            response = []

                            response[0] = LIST + " "
                            response[1] = address

                            for iname, iaddress in self.members.iteritems():
                                if (iname != name):
                                    response[0] = response[0] + iname + " "

                            self.outgoing_lock.acquire()
                            
                            print "Server.process_in(): Acquired outgoing_lock"

                            self.outgoing.append(response)

                            self.outgoing_lock.release()
                            print "Server.process_in(): Released outgoing_lock"

                if (data_in[0][:len(SERVERKILL)] == SERVERKILL):
                    print "Server.process_in(): Received SERVERKILL"

                    if (data_in[0][len(SERVERKILL) + 1:] == self.configuration['password']):
                        self.running = 0

                if (data_in[0][:len(LETTER)] == LETTER):
                    print "Server.process_in(): Received LETTER"

                    responses = []

                    the_data = data_in[0]
                    it_is = ''

                    origin = ''

                    for name in self.members.keys():
                        if self.members[name] == data_in[1]:
                            origin = name

                    if origin != '':

                        response = [LETTER + ":" + origin + ":", []]

                        if the_data[len(LETTER):len(LETTER)+2] == '::':

                            it_is = the_data[the_data.find("::") + 2:]
                            response[0] += it_is

                            for name in self.members.keys():

                                if origin != name:

                                    responses.append(response)
                                    responses[-1][1] = self.members[name]

                        else:

                            it_is = the_data[the_data.find(":", the_data.find(":") + 1) + 1:]
                            response[0] += it_is

                            string_parts = the_data[the_data.find(":") + 1:
                                                    the_data.find(":", the_data.find(":") + 1)].split()

                            for dest in string_parts:

                                if self.members.has_key(dest):

                                    responses.append(response)
                                    responses[-1][1] = self.members[dest]

                        for response_i in responses:

                            self.outgoing_lock.acquire()
                            self.outgoing.append(response_i)
                            self.outgoing_lock.release()

        return

    def process_out(self):

        print "Server.process_out(): Not implemented yet."

        return

    def broadcast_listen(self):

        print "Listening on broadcast port:", self.configuration['broadcast']

        broadcast_message = SERVEROFFER + " " + str(self.configuration['message'])
        broadcast_message = broadcast_message + " " + self.configuration['name']

        while (self.running):
            data_in = self.broadcast.recvfrom(BUFSIZE)

            if (data_in[0][0:len(SERVERREQUEST)] == SERVERREQUEST):
                new_message = [broadcast_message]
                new_message.append((data_in[1][0], int(data_in[0][len(SERVERREQUEST) + 1:])))

                self.outgoing_lock.acquire()
                print "Server.broadcast_listen(): Acquired outgoing_lock"

                self.outgoing.append(new_message)

                self.outgoing_lock.release()
                print "Server.broadcast_listen(): Released outgoing_lock"

        return

    def end_it(self):
        self.running = 0
        return

def parse_config(filename):

    parsed = {'error': 1}

    if (exists(filename)):
        dom = parse(filename)
    else:
        print "parse_config():", filename, "doesn't exist"
        return parsed

    if (dom.childNodes[0].nodeName == 'server_config'):
        for node in dom.childNodes[0].childNodes:

            if (node.nodeName == 'name' and len(node.childNodes)):
                parsed['name'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'password' and len(node.childNodes)):
                parsed['password'] = node.childNodes[0].nodeValue

            if (node.nodeName == 'broadcast' and len(node.childNodes)):
                parsed['broadcast'] = int(node.childNodes[0].nodeValue)

            if (node.nodeName == 'message' and len(node.childNodes)):
                parsed['message'] = int(node.childNodes[0].nodeValue)

    parsed['error'] = 0

    return parsed

def run(configuration):

    my_server = Server(configuration)

    receive_thread = threading.Thread(target=my_server.receive)
    send_thread = threading.Thread(target=my_server.send)
    process_in_thread = threading.Thread(target=my_server.process_in)
    process_out_thread = threading.Thread(target=my_server.process_out)
    broadcast_thread = threading.Thread(target=my_server.broadcast_listen)

    receive_thread.start()
    send_thread.start()
    broadcast_thread
    process_in_thread.start()
    process_out_thread.start()
    broadcast_thread.start()

    receive_thread.join()
    send_thread.join()
    broadcast_thread.join()
    process_in_thread.join()
    process_out_thread.join()

    return

def handler(signal, frame):
    if (signal == SIGINT):
        my_server.end_it()
        print "It should have ended"

def main():

    # parsing command line options

    if (len(sys.argv) > 1):
        config_file = sys.argv[1]
    else:
        print "main(): No config file specified, using", CONFIG
        config_file = CONFIG

    parsed_config = parse_config(config_file)

    if (parsed_config['error']):
        print "main(): Error parsing config, ending"
        return

    signal(SIGINT, handler)

    run(parsed_config)

if __name__ == '__main__':
    main()
