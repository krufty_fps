# This is the netscript

# each script must define this
global the_functions
global in_script

global server_count
server_count = 0

global QUITD
global SERVEROFFERD
global KEYDOWND
global WIDGET_CLICKEDD
global LISTD
global YOUREIND
global SOMEONEJOINEDD
global SOMEONELEFTD
global YOUROUTD
global SERVER_GONED
global LETTERD
global READYD

def QUITD(data):

    print "This is the net script, and we are quitting the game."
    return

def SERVEROFFERD(data):
    global the_engine
    global server_count

    server_count = server_count + 1

    new_string = ''.join([data['server_name'], " ", str(data['server_address'])])

    the_engine.addListItem('lan_menu', 'server_list', new_string,
                           dvalue=data)

    return

def SERVER_GONED(data):

    visible_menus = the_engine.getVisibleMenus()

    if visible_menus == None:
        return

    if 'net_menu' in visible_menus.keys():
        the_client.executeCommand(IMOUT, {})
        the_engine.setMenu('lan_menu')

        event = ['WIDGET_CLICKED', {'menu_name': 'lan_menu', 'name': 'refresh_button'}]

        the_engine.events.addEvent(event)
        the_engine.clearListItem('lan_menu', 'list_server')

    return

def KEYDOWND(data):

    visible_menus = the_engine.getVisibleMenus()

    if visible_menus == None:
        return

    if 'net_menu' in visible_menus.keys():
        if data.has_key('event'):
            if data['event'][1]['key'] == 13:
                if the_engine.getEditboxValue('net_menu', 'chat') != '':

                    the_message = "MESSAGE " + the_engine.getEditboxValue('net_menu', 'chat')
                    the_client.executeCommand(LETTER,
                                              {'destinations': [],
                                               'message': the_message})
                    the_engine.setEditboxValue('net_menu', 'chat', '')
                    the_engine.addListItem('net_menu', 'chat_list', ''.join([the_engine.config['name'], ": ", the_message[len('MESSAGE '):]]), 'Firey')

    return

def LETTERD(data):

    visible_menus = the_engine.getVisibleMenus()

    if visible_menus == None:
        return

    if 'net_menu' in visible_menus.keys():

        split_string = data['message']

        if data['message'][:len('MESSAGE')] == 'MESSAGE':
            the_message = data['message'][len('MESSAGE') + 1:]

            the_engine.addListItem('net_menu', 'chat_list', ''.join([data['origin'], ':', the_message]), the_message)

    return

def WIDGET_CLICKEDD(data):

    global the_engine
    global the_client

    if data['menu_name'] == 'lan_menu':

        if data['name'] == 'refresh_button':

            the_engine.clearListItem('lan_menu', 'server_list')
            the_client.executeCommand(SERVERREQUEST, {})

        if data['name'] == 'server_list':

            list_value = the_engine.getListValue('lan_menu', 'server_list')

            if list_value:

                the_client.executeCommand(GETLIST, {'server': list_value['server_name']})

            print "The server selected is", the_engine.getListValue('lan_menu', 'server_list')

        if data['name'] == 'join_button':

            list_value = the_engine.getListValue('lan_menu', 'server_list')

            if (list_value != None):
                the_client.executeCommand(WANTIN, {'server': list_value['server_name']})

    if data['menu_name'] == 'net_menu':

        if data['name'] == 'back_button':

            the_client.executeCommand(IMOUT, {})
            the_engine.setMenu('lan_menu')

            event = ['WIDGET_CLICKED', {'menu_name': 'lan_menu', 'name': 'refresh_button'}]

            the_engine.events.addEvent(event)
            the_engine.clearListItem('lan_menu', 'list_server')

    return

def LISTD(data):

    visible_menus = the_engine.getVisibleMenus()

    if visible_menus == None:
        return

    if 'net_menu' in visible_menus.keys():

        the_engine.clearListItem('net_menu', 'player_list')

        if len(data['names']):
            for name in data['names']:

                if name != the_engine.config['name']:
                    the_engine.addListItem('net_menu', 'player_list', name, name)
                else:
                    the_engine.addListItem('net_menu', 'player_list', ''.join([name, ' (you)']), name)

    if 'lan_menu' in visible_menus.keys():
        list_value = the_engine.getListValue('lan_menu', 'server_list')

        if list_value['server_address'] == data['server_address']:

            the_engine.clearListItem('lan_menu', 'list_server')

            the_info = []

            the_info.append(''.join(['Server Name: ', list_value['server_name']]))

            if len(data['names']):
                the_info.append(''.join(['Members:']))

                for name in data['names']:
                    the_info.append(''.join(['\t', name]))
            else:
                the_info.append('No members')

            the_info.append('Other metainformation isn\'t supported yet.')

            for item in the_info:
                the_engine.addListItem('lan_menu', 'list_server', item, item)

        else:
            the_engine.clearListItem('lan_menu', 'list_server')

    return

def SOMEONEJOINEDD(data):

    visible_menus = the_engine.getVisibleMenus()

    if visible_menus == None:
        return

    if 'net_menu' in visible_menus:

        the_engine.addListItem('net_menu', 'player_list', data['name'], data['name'])
        the_engine.addListItem('net_menu', 'chat_list', ''.join([data['name'], " joined server."]), data['name'] + 'd')

    return

def SOMEONELEFTD(data):

    visible_menus = the_engine.getVisibleMenus()

    if visible_menus == None:
        return

    if 'net_menu' in visible_menus:

        the_engine.removeListItem('net_menu', 'player_list', data['name'])

    return

def YOUREIND(data):

    the_engine.setMenu('net_menu')

    visible_menus = the_engine.getVisibleMenus()

    if visible_menus == None:
        return

    if 'net_menu' in visible_menus.keys():
        the_client.executeCommand(GETLIST, {})

    return

def YOUROUTD(data):

    visible_menus = the_engine.getVisibleMenus()

    if visible_menus == None:
        return

    if 'net_menu' in visible_menus:
        
        the_client.executeCommand(IMOUT, {})
        the_engine.setMenu('lan_menu')

        event = ['WIDGET_CLICKED', {'menu_name': 'lan_menu', 'name': 'refresh_button'}]

        the_engine.events.addEvent(event)
        the_engine.clearListItem('lan_menu', 'list_server')

    return

def READYD(data):
    global server_count
    
    if team1 == 1 and team2 == 1: # These are dummy variables until I get real ones from the gui
       start_game = 1 # This will call something in the gui eventually

if in_script:

    the_functions['KEYDOWN'] = KEYDOWND
    the_functions['QUIT'] = QUITD
    the_functions[SERVEROFFER] = SERVEROFFERD
    the_functions['WIDGET_CLICKED'] = WIDGET_CLICKEDD
    the_functions[LIST] = LISTD
    the_functions[YOUREIN] = YOUREIND
    the_functions[SOMEONEJOINED] = SOMEONEJOINEDD
    the_functions[SOMEONELEFT] = SOMEONELEFTD
    the_functions[YOUROUT] = YOUROUTD
    the_functions[SERVER_GONE] = SERVER_GONED
    the_functions[LETTER] = LETTERD

del YOUROUTD
del LISTD
del WIDGET_CLICKEDD
del KEYDOWND
del SERVEROFFERD
del QUITD
del YOUREIND
del SOMEONELEFTD
del SOMEONEJOINEDD
del SERVER_GONED
del LETTERD
del READYD

# end
